package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.IEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.util.EndpointUtil;

import java.util.Collection;

@Component
@RequiredArgsConstructor
public final class Bootstrap {

    @NotNull
    private final IConfigurationService configurationService;

    @NotNull
    private final Collection<IEndpoint> endpoints;

    public void run() {
        publishWebServices();
    }

    private void publishWebServices() {
        @Nullable final String host = this.configurationService.getServerHost();
        @Nullable final Integer port = this.configurationService.getServerPort();
        EndpointUtil.publish(this.endpoints, host, port);
    }

}