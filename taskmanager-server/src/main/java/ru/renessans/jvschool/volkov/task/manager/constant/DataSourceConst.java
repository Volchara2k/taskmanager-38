package ru.renessans.jvschool.volkov.task.manager.constant;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;

public interface DataSourceConst {

    @NotNull
    String DRIVER = Environment.DRIVER;

    @NotNull
    String URL = Environment.URL;

    @NotNull
    String USER = Environment.USER;

    @NotNull
    String PASS = Environment.PASS;

    @NotNull
    String SHOW_SQL = Environment.SHOW_SQL;

    @NotNull
    String FORMAT_SQL = Environment.FORMAT_SQL;

    @NotNull
    String HBM2DDL_AUTO = Environment.HBM2DDL_AUTO;

    @NotNull
    String DIALECT = Environment.DIALECT;

}