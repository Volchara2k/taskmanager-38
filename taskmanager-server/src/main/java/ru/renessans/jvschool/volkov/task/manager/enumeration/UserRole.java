package ru.renessans.jvschool.volkov.task.manager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum UserRole {

    @NotNull
    ADMIN("Администратор"),

    @NotNull
    USER("Пользователь"),

    @NotNull
    UNKNOWN("Неизвестный");

    @NotNull
    private final String title;

    UserRole(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return title;
    }

    public boolean isAdmin() {
        return this == ADMIN;
    }

    public boolean isUser() {
        return this == USER;
    }

}