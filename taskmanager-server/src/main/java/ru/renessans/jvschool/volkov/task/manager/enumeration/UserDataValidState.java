package ru.renessans.jvschool.volkov.task.manager.enumeration;

import org.jetbrains.annotations.NotNull;

public enum UserDataValidState {

    @NotNull
    SUCCESS("Успешно!"),

    @NotNull
    USER_NOT_FOUND("Пользователь не найден!"),

    @NotNull
    INVALID_PASSWORD("Некорректный пароль!"),

    @NotNull
    LOCKDOWN_PROFILE("Профиль заблокирован!");

    @NotNull
    private final String title;

    UserDataValidState(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

    public boolean isNotSuccess() {
        return this != SUCCESS;
    }

}