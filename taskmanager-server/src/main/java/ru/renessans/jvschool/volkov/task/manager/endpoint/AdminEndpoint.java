package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.*;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserLimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Collection;
import java.util.stream.Collectors;

@WebService
@Controller
@RequiredArgsConstructor
public final class AdminEndpoint implements IAdminEndpoint {

    @NotNull
    private static final UserRole ROLE = UserRole.ADMIN;

    @NotNull
    private final IAuthenticationService authenticationService;

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final IConfigurationService configurationService;

    @NotNull
    private final ISessionAdapterService sessionAdapterService;

    @NotNull
    private final IUserLimitedAdapterService userLimitedAdapterService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @WebMethod
    @WebResult(name = "serverData", partName = "serverData")
    @Override
    public String serverData(
            @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session open = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(open, ROLE);
        return String.format(
                "Хост: %s\nпорт: %d",
                this.configurationService.getServerHost(), this.configurationService.getServerPort()
        );
    }

    @WebMethod
    @WebResult(name = "closedAllSessions", partName = "closedAllSessions")
    @Override
    public int closedAllSessions(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session open = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(open, ROLE);
        return this.sessionService.closeAllSessions(open);
    }

    @WebMethod
    @WebResult(name = "sessions", partName = "sessions")
    @NotNull
    @Override
    public Collection<SessionDTO> getAllSessions(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.sessionService.exportRecords()
                .stream()
                .map(this.sessionAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO signUpUserWithUserRole(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "userRole", partName = "userRole") @Nullable final UserRole role
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        @NotNull final User user = this.authenticationService.signUp(login, password, role);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.userService.deleteUserById(id);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session open = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(open, ROLE);
        return this.userService.deleteUserByLogin(login);
    }

    @WebMethod
    @WebResult(name = "deleteFlag", partName = "deleteFlag")
    @Override
    public int deleteAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session open = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(open, ROLE);
        return this.userService.cascadeDeleteAllRecords();
    }

    @WebMethod
    @WebResult(name = "lockedUser", partName = "lockedUser")
    @Nullable
    @Override
    public UserLimitedDTO lockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        @Nullable final User user = this.userService.lockUserByLogin(login);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "unlockedUser", partName = "unlockedUser")
    @Nullable
    @Override
    public UserLimitedDTO unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        @Nullable final User user = this.userService.unlockUserByLogin(login);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "users", partName = "users")
    @Nullable
    @Override
    public Collection<UserLimitedDTO> setAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "users", partName = "users") @Nullable final Collection<UserLimitedDTO> usersDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        this.userService.importRecords(usersDTO != null ? usersDTO
                .stream()
                .map(this.userLimitedAdapterService::toModel)
                .collect(Collectors.toList()) : null);
        return usersDTO;
    }

    @WebMethod
    @WebResult(name = "users", partName = "users")
    @NotNull
    @Override
    public Collection<UserLimitedDTO> getAllUsers(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.userService.exportRecords()
                .stream()
                .map(this.userLimitedAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @NotNull
    @Override
    public Collection<TaskDTO> getAllUsersTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.taskUserService.exportRecords()
                .stream()
                .map(this.taskAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "tasks", partName = "tasks")
    @Nullable
    @Override
    public Collection<TaskDTO> setAllUsersTasks(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "tasks", partName = "tasks") @Nullable final Collection<TaskDTO> tasksDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        this.taskUserService.importRecords(tasksDTO != null ? tasksDTO
                .stream()
                .map(this.taskAdapterService::toModel)
                .collect(Collectors.toList()) : null);
        return tasksDTO;
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @NotNull
    @Override
    public Collection<ProjectDTO> getAllUsersProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.projectUserService.exportRecords()
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @WebMethod
    @WebResult(name = "projects", partName = "projects")
    @Nullable
    @Override
    public Collection<ProjectDTO> setAllUsersProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "projects", partName = "projects") @Nullable final Collection<ProjectDTO> projectsDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        this.projectUserService.importRecords(projectsDTO != null ? projectsDTO
                .stream()
                .map(this.projectAdapterService::toModel)
                .collect(Collectors.toList()) : null);
        return projectsDTO;
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO getUserById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        @Nullable final User user = this.userService.getUserById(id);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @Override
    public UserLimitedDTO getUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        @Nullable final User user = this.userService.getUserByLogin(login);
        @Nullable final UserLimitedDTO userLimitedDTO = this.userLimitedAdapterService.toDTO(user);
        return userLimitedDTO;
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserLimitedDTO editProfileById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        @Nullable final User user = this.userService.editUserProfileById(id, firstName);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "editedUser", partName = "editedUser")
    @Nullable
    @Override
    public UserLimitedDTO editProfileByIdWithLastName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        @Nullable final User user = this.userService.editUserProfileById(id, firstName, lastName);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "updatedUser", partName = "updatedUser")
    @Nullable
    @Override
    public UserLimitedDTO updatePasswordById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        @Nullable final User user = this.userService.updateUserPasswordById(id, newPassword);
        return this.userLimitedAdapterService.toDTO(user);
    }

}