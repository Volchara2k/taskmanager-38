package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IDataInterChangeService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ISessionService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ISessionAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService
@Controller
@RequiredArgsConstructor
public final class AdminDataInterChangeEndpoint implements IAdminDataInterChangeEndpoint {

    @NotNull
    private static final UserRole ROLE = UserRole.ADMIN;

    @NotNull
    private final IDataInterChangeService dataInterChangeService;

    @NotNull
    private final ISessionAdapterService sessionAdapterService;

    @NotNull
    private final ISessionService sessionService;

    @WebMethod
    @WebResult(name = "clearedBinData", partName = "clearedBinData")
    @Override
    public boolean dataBinClear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.dataBinClear();
    }

    @WebMethod
    @WebResult(name = "clearedBase64Data", partName = "clearedBase64Data")
    @Override
    public boolean dataBase64Clear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.dataBase64Clear();
    }

    @WebMethod
    @WebResult(name = "clearedJsonData", partName = "clearedJsonData")
    @Override
    public boolean dataJsonClear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.dataJsonClear();
    }

    @WebMethod
    @WebResult(name = "clearedXmlData", partName = "clearedXmlData")
    @Override
    public boolean dataXmlClear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.dataXmlClear();
    }

    @WebMethod
    @WebResult(name = "clearedYamlData", partName = "clearedYamlData")
    @Override
    public boolean dataYamlClear(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.dataYamlClear();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataBin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.exportDataBin();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.exportDataBase64();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.exportDataJson();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.exportDataXml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO exportDataYaml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.exportDataYaml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataBin(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.importDataBin();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataBase64(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.importDataBase64();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataJson(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.importDataJson();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataXml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.importDataXml();
    }

    @WebMethod
    @WebResult(name = "domain", partName = "domain")
    @NotNull
    @Override
    public DomainDTO importDataYaml(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) {
        @Nullable final Session conversion = this.sessionAdapterService.toModel(sessionDTO);
        @NotNull final Session current = this.sessionService.validateSession(conversion);
        this.sessionService.validateSession(current, ROLE);
        return this.dataInterChangeService.importDataYaml();
    }

}