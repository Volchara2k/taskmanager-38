package ru.renessans.jvschool.volkov.task.manager.exception.invalid.hash;

import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.exception.AbstractException;

public final class InvalidSignatureCycleException extends AbstractException {

    @NotNull
    private static final String EMPTY_CYCLE = "Ошибка! Параметр \"цикл для подписи\" отсутствует!\n";

    public InvalidSignatureCycleException() {
        super(EMPTY_CYCLE);
    }

}