package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserDataValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidFirstNameException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidLoginException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidPasswordException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserRoleException;
import ru.renessans.jvschool.volkov.task.manager.model.User;
import ru.renessans.jvschool.volkov.task.manager.util.HashUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public final class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;

    @NotNull
    @SneakyThrows
    @Override
    public PermissionValidState verifyValidPermission(
            @Nullable final String userId,
            @Nullable final UserRole requiredRole
    ) {
        if (Objects.isNull(requiredRole)) return PermissionValidState.SUCCESS;
        @NotNull final UserRole userRole = this.userService.getUserRole(userId);

        final boolean isOnlyUnknownCommand = requiredRole.equals(UserRole.UNKNOWN);
        final boolean isAuthedUserRole = (userRole != UserRole.UNKNOWN);
        if (isOnlyUnknownCommand && isAuthedUserRole) return PermissionValidState.LOGGED;

        boolean containsTypes = requiredRole.equals(userRole);
        if (containsTypes) return PermissionValidState.SUCCESS;

        final boolean isOnlyAdminCommand = requiredRole.equals(UserRole.ADMIN);
        final boolean isUserUserRole = (userRole == UserRole.USER);
        if (isOnlyAdminCommand && isUserUserRole) return PermissionValidState.NO_ACCESS_RIGHTS;

        return PermissionValidState.NEED_LOG_IN;
    }

    @NotNull
    @SneakyThrows
    @Override
    public UserDataValidState verifyValidUserData(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();

        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) return UserDataValidState.USER_NOT_FOUND;
        if (user.getLockdown()) return UserDataValidState.LOCKDOWN_PROFILE;

        @NotNull final String passwordHash = HashUtil.getSaltHashLine(password);
        final boolean isNotEqualPasswords = !passwordHash.equals(user.getPasswordHash());
        if (isNotEqualPasswords) return UserDataValidState.INVALID_PASSWORD;

        return UserDataValidState.SUCCESS;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        return this.userService.addUser(login, password);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String firstName
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (ValidRuleUtil.isNullOrEmpty(firstName)) throw new InvalidFirstNameException();
        return this.userService.addUser(login, password, firstName);
    }

    @NotNull
    @SneakyThrows
    @Override
    public User signUp(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final UserRole role
    ) {
        if (ValidRuleUtil.isNullOrEmpty(login)) throw new InvalidLoginException();
        if (ValidRuleUtil.isNullOrEmpty(password)) throw new InvalidPasswordException();
        if (Objects.isNull(role)) throw new InvalidUserRoleException();
        return this.userService.addUser(login, password, role);
    }

}