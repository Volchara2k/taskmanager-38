package ru.renessans.jvschool.volkov.task.manager.service.adapter;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TimeFrameDTO;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.TimeFrame;

import java.util.Objects;

@Service
public final class ProjectAdapterService implements IProjectAdapterService {

    @Nullable
    @Override
    public ProjectDTO toDTO(@Nullable final Project convertible) {
        if (Objects.isNull(convertible)) return null;
        return ProjectDTO.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(
                        TimeFrameDTO.builder()
                                .creationDate(convertible.getTimeFrame().getCreationDate())
                                .build()
                )
                .status(convertible.getStatus())
                .build();
    }

    @Nullable
    @Override
    public Project toModel(@Nullable final ProjectDTO convertible) {
        if (Objects.isNull(convertible)) return null;
        return Project.builder()
                .id(convertible.getId())
                .userId(convertible.getUserId())
                .title(convertible.getTitle())
                .description(convertible.getDescription())
                .timeFrame(
                        TimeFrame.builder()
                                .creationDate(convertible.getTimeFrame().getCreationDate())
                                .build()
                )
                .status(convertible.getStatus())
                .build();
    }

}