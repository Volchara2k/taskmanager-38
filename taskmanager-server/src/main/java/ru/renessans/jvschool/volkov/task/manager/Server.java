package ru.renessans.jvschool.volkov.task.manager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import ru.renessans.jvschool.volkov.task.manager.bootstrap.Bootstrap;
import ru.renessans.jvschool.volkov.task.manager.configuration.DataSourceConfiguration;

public final class Server {

    public static void main(@Nullable final String[] args) {
        @NotNull final AbstractApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(DataSourceConfiguration.class);
        applicationContext.registerShutdownHook();
        @NotNull final Bootstrap bootstrap = applicationContext.getBean(Bootstrap.class);
        bootstrap.run();
    }

}