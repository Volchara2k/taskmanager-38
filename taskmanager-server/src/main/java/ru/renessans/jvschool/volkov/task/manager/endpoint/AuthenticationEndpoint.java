package ru.renessans.jvschool.volkov.task.manager.endpoint;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.renessans.jvschool.volkov.task.manager.api.endpoint.IAuthenticationEndpoint;
import ru.renessans.jvschool.volkov.task.manager.api.service.IAuthenticationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IUserLimitedAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.PermissionValidState;
import ru.renessans.jvschool.volkov.task.manager.enumeration.UserRole;
import ru.renessans.jvschool.volkov.task.manager.exception.security.AccessFailureException;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.Objects;

@WebService
@Controller
@RequiredArgsConstructor
public final class AuthenticationEndpoint implements IAuthenticationEndpoint {

    @NotNull
    private final IAuthenticationService authenticationService;

    @NotNull
    private final IUserLimitedAdapterService userLimitedAdapterService;

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public UserLimitedDTO signUpUser(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        @Nullable String userId = null;
        if (!Objects.isNull(sessionDTO)) userId = sessionDTO.getUserId();
        @NotNull final PermissionValidState permissionValidState = this.authenticationService.verifyValidPermission(userId, UserRole.UNKNOWN);
        if (permissionValidState.isNotSuccess()) throw new AccessFailureException(permissionValidState.getTitle());
        @NotNull final User user = this.authenticationService.signUp(login, password);
        return this.userLimitedAdapterService.toDTO(user);
    }

    @WebMethod
    @WebResult(name = "user", partName = "user")
    @Nullable
    @SneakyThrows
    @Override
    public UserLimitedDTO signUpUserWithFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @Nullable String userId = null;
        if (!Objects.isNull(sessionDTO)) userId = sessionDTO.getUserId();
        @NotNull final PermissionValidState permissionValidState = this.authenticationService.verifyValidPermission(userId, UserRole.UNKNOWN);
        if (permissionValidState.isNotSuccess()) throw new AccessFailureException(permissionValidState.getTitle());
        @NotNull final User user = this.authenticationService.signUp(login, password, firstName);
        return this.userLimitedAdapterService.toDTO(user);
    }

}