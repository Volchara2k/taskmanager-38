package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IUserService;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataBaseProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataProjectProvider;
import ru.renessans.jvschool.volkov.task.manager.casedata.CaseDataUserProvider;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.marker.NegativeImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.PositiveImplementation;
import ru.renessans.jvschool.volkov.task.manager.marker.ServiceImplementation;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.model.User;

import java.util.Collection;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(value = JUnitParamsRunner.class)
public final class ProjectUserServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService(PROPERTY_SERVICE);

    @NotNull
    private static final IUserService USER_SERVICE = new UserService(null);

    @NotNull
    private static final IProjectUserService PROJECT_SERVICE = new ProjectUserService(null, null);

    @BeforeClass
    public static void preparingConfigurationBefore() {
        Assert.assertNotNull(PROPERTY_SERVICE);
        Assert.assertNotNull(CONFIG_SERVICE);
        PROPERTY_SERVICE.load();
    }

    @BeforeClass
    public static void assertMainComponentsNotNullBefore() {
        Assert.assertNotNull(USER_SERVICE);
        Assert.assertNotNull(PROJECT_SERVICE);
    }

    @Test
    @TestCaseName("Run testNegativeAdd for add(\"{0}\", \"{1}\", \"{2}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerMainFieldsCaseData"
    )
    public void testNegativeAdd(
            @Nullable final String userId,
            @Nullable final String title,
            @Nullable final String description
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.addOwnerUser(userId, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> PROJECT_SERVICE.addOwnerUser(userIdTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = UUID.randomUUID().toString();
        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
                InvalidDescriptionException.class,
                () -> PROJECT_SERVICE.addOwnerUser(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeUpdateByIndex for updateByIndex(\"{0}\", {1}, \"{2}\", \"{3}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerWithIndexCaseData"
    )
    public void testNegativeUpdateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String title,
            @Nullable final String description
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.updateOwnerUserByIndex(userId, index, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> PROJECT_SERVICE.updateOwnerUserByIndex(userIdTemp, index, title, description)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());

        @NotNull final Integer indexTemp = 0;
        Assert.assertNotNull(indexTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> PROJECT_SERVICE.updateOwnerUserByIndex(userIdTemp, indexTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = UUID.randomUUID().toString();
        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
                InvalidDescriptionException.class,
                () -> PROJECT_SERVICE.addOwnerUser(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeUpdateById for updateById(\"{0}\", \"{1}\", \"{2}\", \"{3}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerWithIdCaseData"
    )
    public void testNegativeUpdateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String title,
            @Nullable final String description
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.updateOwnerUserById(userId, id, title, description)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidIdException indexThrown = assertThrows(
                InvalidIdException.class,
                () -> PROJECT_SERVICE.updateOwnerUserById(userIdTemp, id, title, description)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());

        @NotNull final String idTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(idTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> PROJECT_SERVICE.updateOwnerUserById(userIdTemp, idTemp, title, description)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());

        @NotNull final String titleTemp = UUID.randomUUID().toString();
        @NotNull final InvalidDescriptionException descriptionThrown = assertThrows(
                InvalidDescriptionException.class,
                () -> PROJECT_SERVICE.addOwnerUser(userIdTemp, titleTemp, description)
        );
        Assert.assertNotNull(descriptionThrown);
        Assert.assertNotNull(descriptionThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteByIndex for deleteByIndex(\"{0}\", {1})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerIndexCaseData"
    )
    public void testNegativeDeleteByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.deleteOwnerUserByIndex(userId, index)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> PROJECT_SERVICE.deleteOwnerUserByIndex(userIdTemp, index)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteById for deleteById(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeDeleteById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.deleteOwnerUserById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidIdException idThrown = assertThrows(
                InvalidIdException.class,
                () -> PROJECT_SERVICE.deleteOwnerUserById(userIdTemp, id)
        );
        Assert.assertNotNull(idThrown);
        Assert.assertNotNull(idThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeDeleteByTitle for deleteByTitle(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeDeleteByTitle(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.deleteOwnerUserById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> PROJECT_SERVICE.deleteOwnerUserByTitle(userIdTemp, id)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());
    }

    @Test(expected = InvalidUserIdException.class)
    @TestCaseName("Run testNegativeDeleteAll for deleteAll(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeDeleteAll(
            @Nullable final String userId
    ) {
        PROJECT_SERVICE.deleteOwnerUserAll(userId);
    }

    @Test
    @TestCaseName("Run testNegativeGetByIndex for getByIndex(\"{0}\", {1})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidOwnerIndexCaseData"
    )
    public void testNegativeGetByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.getOwnerUserByIndex(userId, index)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final IllegalIndexException indexThrown = assertThrows(
                IllegalIndexException.class,
                () -> PROJECT_SERVICE.getOwnerUserByIndex(userIdTemp, index)
        );
        Assert.assertNotNull(indexThrown);
        Assert.assertNotNull(indexThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeGetById for getById(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeGetById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.getOwnerUserById(userId, id)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidIdException idThrown = assertThrows(
                InvalidIdException.class,
                () -> PROJECT_SERVICE.getOwnerUserById(userIdTemp, id)
        );
        Assert.assertNotNull(idThrown);
        Assert.assertNotNull(idThrown.getMessage());
    }

    @Test
    @TestCaseName("Run testNegativeGetByTitle for getByTitle(\"{0}\", \"{1}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidUsersEditableCaseData"
    )
    public void testNegativeGetByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        @NotNull final InvalidUserIdException userIdThrown = assertThrows(
                InvalidUserIdException.class,
                () -> PROJECT_SERVICE.getOwnerUserByTitle(userId, title)
        );
        Assert.assertNotNull(userIdThrown);
        Assert.assertNotNull(userIdThrown.getMessage());

        @NotNull final String userIdTemp = UUID.randomUUID().toString();
        Assert.assertNotNull(userIdTemp);
        @NotNull final InvalidTitleException titleThrown = assertThrows(
                InvalidTitleException.class,
                () -> PROJECT_SERVICE.getOwnerUserByTitle(userIdTemp, title)
        );
        Assert.assertNotNull(titleThrown);
        Assert.assertNotNull(titleThrown.getMessage());
    }

    @Test(expected = InvalidUserIdException.class)
    @TestCaseName("Run testNegativeGetAll for getAll(\"{0}\")")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataBaseProvider.class,
            method = "invalidLinesCaseData"
    )
    public void testNegativeGetAll(
            @Nullable final String userId
    ) {
        PROJECT_SERVICE.getOwnerUserAll(userId);
    }

    @Test(expected = InvalidUserException.class)
    @TestCaseName("Run testInitialDemoData for initialDemoData({0})")
    @Category({NegativeImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "invalidCollectionsUsersCaseData"
    )
    public void testNegativeInitialDemoData(
            @Nullable final Collection<User> users
    ) {
        PROJECT_SERVICE.initialOwnerUser(users);
    }

    @Test
    @TestCaseName("Run testAdd for add({0}, {1})")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataProjectProvider.class,
            method = "validProjectsCaseData"
    )
    public void testAdd(
            @NotNull final User user,
            @NotNull final Project project
    ) {
        Assert.assertNotNull(user);
        Assert.assertNotNull(project);
        @NotNull final Project addProject = PROJECT_SERVICE.addOwnerUser(user.getId(), project.getTitle(), project.getDescription());
        Assert.assertNotNull(addProject);
        Assert.assertEquals(project.getUserId(), addProject.getUserId());
        Assert.assertEquals(project.getTitle(), addProject.getTitle());
        Assert.assertEquals(project.getDescription(), addProject.getDescription());
        @Nullable final Project cleared = PROJECT_SERVICE.deleteRecord(addProject);
        Assert.assertNotNull(cleared);
    }

//    @Test
//    @TestCaseName("Run testUpdateByIndex for updateByIndex({0}, 0, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testUpdateByIndex(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @NotNull final String newData = UUID.randomUUID().toString();
//        Assert.assertNotNull(newData);
//        @Nullable final Project updateProject = PROJECT_SERVICE.updateByIndex(addUser.getId(), 0, newData, newData);
//        Assert.assertNotNull(updateProject);
//        Assert.assertEquals(addProject.getId(), updateProject.getId());
//        Assert.assertEquals(addProject.getUserId(), updateProject.getUserId());
//        Assert.assertEquals(newData, updateProject.getTitle());
//        Assert.assertEquals(newData, updateProject.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testUpdateById for updateById({0}, {1}")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testUpdateById(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @NotNull final String newData = UUID.randomUUID().toString();
//        Assert.assertNotNull(newData);
//        @Nullable final Project updateProject = PROJECT_SERVICE.updateById(addUser.getId(), addProject.getId(), newData, newData);
//        Assert.assertNotNull(updateProject);
//        Assert.assertEquals(addProject.getId(), updateProject.getId());
//        Assert.assertEquals(addProject.getUserId(), updateProject.getUserId());
//        Assert.assertEquals(newData, updateProject.getTitle());
//        Assert.assertEquals(newData, updateProject.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteByIndex for deleteByIndex({0}, 0)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testDeleteByIndex(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @Nullable final Project deleteProject = PROJECT_SERVICE.deleteByIndex(addUser.getId(), 0);
//        Assert.assertNotNull(deleteProject);
//        Assert.assertEquals(addProject.getId(), deleteProject.getId());
//        Assert.assertEquals(addProject.getUserId(), deleteProject.getUserId());
//        Assert.assertEquals(addProject.getTitle(), deleteProject.getTitle());
//        Assert.assertEquals(addProject.getDescription(), deleteProject.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteById for deleteById({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testDeleteById(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @Nullable final Project deleteProject = PROJECT_SERVICE.deleteById(addUser.getId(), addProject.getId());
//        Assert.assertNotNull(deleteProject);
//        Assert.assertEquals(addProject.getId(), deleteProject.getId());
//        Assert.assertEquals(addProject.getUserId(), deleteProject.getUserId());
//        Assert.assertEquals(addProject.getTitle(), deleteProject.getTitle());
//        Assert.assertEquals(addProject.getDescription(), deleteProject.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteByTitle for deleteByTitle({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testDeleteByTitle(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @Nullable final Project deleteProject = PROJECT_SERVICE.deleteByTitle(addUser.getId(), addProject.getTitle());
//        Assert.assertNotNull(deleteProject);
//        Assert.assertEquals(addProject.getId(), deleteProject.getId());
//        Assert.assertEquals(addProject.getUserId(), deleteProject.getUserId());
//        Assert.assertEquals(addProject.getTitle(), deleteProject.getTitle());
//        Assert.assertEquals(addProject.getDescription(), deleteProject.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testDeleteAll for deleteAll({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testDeleteAll(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @Nullable final Collection<Project> deleteProjects = PROJECT_SERVICE.deleteAll(addUser.getId());
//        Assert.assertNotNull(deleteProjects);
//        Assert.assertNotEquals(0, deleteProjects.size());
//        final boolean isUserProjects = deleteProjects.stream().allMatch(entity -> addUser.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserProjects);
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetByIndex for getByIndex({0}, 0)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetByIndex(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @Nullable final Project getProject = PROJECT_SERVICE.getByIndex(addUser.getId(), 0);
//        Assert.assertNotNull(getProject);
//        Assert.assertEquals(addProject.getId(), getProject.getId());
//        Assert.assertEquals(addProject.getUserId(), getProject.getUserId());
//        Assert.assertEquals(addProject.getTitle(), getProject.getTitle());
//        Assert.assertEquals(addProject.getDescription(), getProject.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetById for getById({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetById(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @Nullable final Project getProject = PROJECT_SERVICE.getById(addUser.getId(), addProject.getId());
//        Assert.assertNotNull(getProject);
//        Assert.assertEquals(addProject.getId(), getProject.getId());
//        Assert.assertEquals(addProject.getUserId(), getProject.getUserId());
//        Assert.assertEquals(addProject.getTitle(), getProject.getTitle());
//        Assert.assertEquals(addProject.getDescription(), getProject.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetByTitle for getByTitle({0}, {1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetByTitle(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @Nullable final Project getProject = PROJECT_SERVICE.getByTitle(addUser.getId(), addProject.getTitle());
//        Assert.assertNotNull(getProject);
//        Assert.assertEquals(addProject.getId(), getProject.getId());
//        Assert.assertEquals(addProject.getUserId(), getProject.getUserId());
//        Assert.assertEquals(addProject.getTitle(), getProject.getTitle());
//        Assert.assertEquals(addProject.getDescription(), getProject.getDescription());
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetAll for getAll({0})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataProjectProvider.class,
//            method = "validProjectsCaseData"
//    )
//    public void testGetAll(
//            @NotNull final User user,
//            @NotNull final Project project
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(project);
//        @NotNull final User addUser = USER_SERVICE.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Project addProject = PROJECT_SERVICE.add(addUser.getId(), project.getTitle(), project.getDescription());
//        Assert.assertNotNull(addProject);
//
//        @Nullable final Collection<Project> getProjects = PROJECT_SERVICE.getAll(addUser.getId());
//        Assert.assertNotNull(getProjects);
//        Assert.assertNotEquals(0, getProjects.size());
//        final boolean isUserProject = getProjects.stream().allMatch(entity -> addUser.getId().equals(entity.getUserId()));
//        Assert.assertTrue(isUserProject);
//        @NotNull final User deleteUser = USER_SERVICE.deleteUserById(addUser.getId());
//        Assert.assertNotNull(deleteUser);
//    }

    @Test
    @TestCaseName("Run testInitialDemoData for initialDemoData()")
    @Category({PositiveImplementation.class, ServiceImplementation.class})
    @Parameters(
            source = CaseDataUserProvider.class,
            method = "validCollectionUsersCaseData"
    )
    public void testInitialDemoData(
            @NotNull final Collection<User> users
    ) {
        Assert.assertNotNull(users);

        @NotNull final Collection<Project> initProjects = PROJECT_SERVICE.initialOwnerUser(users);
        Assert.assertNotNull(initProjects);
        Assert.assertNotEquals(0, initProjects.size());
        initProjects.forEach(project -> {
            @Nullable final Project cleared = PROJECT_SERVICE.deleteRecord(project);
            Assert.assertNotNull(cleared);
        });
    }

}