package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;

@RunWith(value = JUnitParamsRunner.class)
public final class SessionServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService(PROPERTY_SERVICE);

//    @NotNull
//    private static final IUserService USER_SERVICE = new UserService();
//
//    @NotNull
//    private static final IAuthenticationService AUTH_SERVICE = new AuthenticationService(USER_SERVICE);
//
//    @NotNull
//    private static final ISessionService SESSION_SERVICE = new SessionService(
//            AUTH_SERVICE, USER_SERVICE, CONFIG_SERVICE
//    );

//    @BeforeClass
//    public static void assertMainComponentsNotNullBefore() {
//        Assert.assertNotNull(USER_SERVICE);
//        Assert.assertNotNull(AUTH_SERVICE);
//    }
//
//    @Before
//    public void loadConfigBefore() {
//        Assert.assertNotNull(CONFIG_SERVICE);
//        CONFIG_SERVICE.load();
//        ENTITY_SERVICE.build();
//    }

//    @Test(expected = AccessFailureException.class)
//    @TestCaseName("Run testNegativeSetSignature for setSignature(null)")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeSetSignature() {
//        Assert.assertNotNull(USER_SERVICE);
//        Assert.assertNotNull(SESSION_SERVICE);
//        SESSION_SERVICE.setSignature(null);
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeOpenSession for openSession(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeOpenSession(
//            @Nullable final String login,
//            @Nullable final String password
//    ) {
//        Assert.assertNotNull(USER_SERVICE);
//        Assert.assertNotNull(SESSION_SERVICE);
//
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> SESSION_SERVICE.openSession(login, password)
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> SESSION_SERVICE.openSession(tempLogin, password)
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//    }
//
//    @Test
//    @TestCaseName("Run testNegativeVerifyValidUserData for verifyValidUserData(\"{0}\", \"{1}\")")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "invalidUsersEditableCaseData"
//    )
//    public void testNegativeValidateUserData(
//            @Nullable final String login,
//            @Nullable final String password
//    ) {
//        @NotNull final InvalidLoginException loginThrown = assertThrows(
//                InvalidLoginException.class,
//                () -> SESSION_SERVICE.verifyValidUserData(login, password)
//        );
//        Assert.assertNotNull(loginThrown);
//        Assert.assertNotNull(loginThrown.getMessage());
//
//        @NotNull final String tempLogin = DemoDataConst.USER_DEFAULT_LOGIN;
//        @NotNull final InvalidPasswordException passwordThrown = assertThrows(
//                InvalidPasswordException.class,
//                () -> SESSION_SERVICE.verifyValidUserData(tempLogin, password)
//        );
//        Assert.assertNotNull(passwordThrown);
//        Assert.assertNotNull(passwordThrown.getMessage());
//    }
//
//    @Test(expected = AccessFailureException.class)
//    @TestCaseName("Run testNegativeValidateSession for validateSession(null)")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    public void testNegativeValidateSession() {
//        SESSION_SERVICE.validateSession(null);
//    }

//    @Test
//    @TestCaseName("Run testNegativeValidateSessionWithRole for validateSession({1}, role)")
//    @Category({NegativeImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testNegativeValidateSessionWithRole(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session addRecord = SESSION_SERVICE.persist(session);
//        Assert.assertNotNull(addRecord);
//
//        @NotNull final AccessFailureException thrownValidSessionState = assertThrows(
//                AccessFailureException.class,
//                () -> SESSION_SERVICE.validateSession(session, UserRole.UNKNOWN)
//        );
//        Assert.assertNotNull(thrownValidSessionState);
//        Assert.assertNotNull(thrownValidSessionState.getMessage());
//
//        @NotNull final Session signatureSession = SESSION_SERVICE.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//
//        @NotNull final AccessFailureException thrownValidPermissionState = assertThrows(
//                AccessFailureException.class,
//                () -> SESSION_SERVICE.validateSession(signatureSession, UserRole.UNKNOWN)
//        );
//        Assert.assertNotNull(thrownValidPermissionState);
//        Assert.assertNotNull(thrownValidPermissionState.getMessage());
//        //final boolean deleteUser = USER_SERVICE.deletedRecord(addUser);
//        //Assert.assertTrue(deleteUser);
//    }

//    @Test
//    @TestCaseName("Run testSetSignature for setSignature({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testSetSignature(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        Assert.assertEquals(session.getId(), signatureSession.getId());
//        Assert.assertEquals(session.getUserId(), signatureSession.getUserId());
//        Assert.assertEquals(session.getTimestamp(), signatureSession.getTimestamp());
//        Assert.assertEquals(session.getSignature(), signatureSession.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testOpenSession for openSession(\"{0}\",\"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testOpenSession(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final Session open = this.sessionService.openSession(login, password);
//        Assert.assertNotNull(open);
//        Assert.assertEquals(addUser.getId(), open.getUserId());
//        @Nullable final Session getSession = this.sessionService.getSessionByUserId(open);
//        Assert.assertNotNull(getSession);
//        Assert.assertEquals(open.getId(), getSession.getId());
//        Assert.assertEquals(open.getUserId(), getSession.getUserId());
//        Assert.assertEquals(open.getTimestamp(), getSession.getTimestamp());
//        Assert.assertEquals(open.getSignature(), getSession.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testCloseSession for closeSession({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testCloseSession(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//
//        final boolean isClosedSession = this.sessionService.closeSession(open);
//        Assert.assertTrue(isClosedSession);
//    }
//
//    @Test
//    @TestCaseName("Run testCloseAllSessions for closeAllSessions({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testCloseAllSessions(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//
//        final boolean isClosedSession = this.sessionService.closeAllSessions(open);
//        Assert.assertTrue(isClosedSession);
//    }
//
//    @Test
//    @TestCaseName("Run testGetSessionByUserId for getSessionByUserId({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testGetSessionByUserId(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
//        Assert.assertNotNull(open);
//
//        @Nullable final Session getSession = this.sessionService.getSessionByUserId(open);
//        Assert.assertNotNull(getSession);
//        Assert.assertEquals(open.getId(), getSession.getId());
//        Assert.assertEquals(open.getUserId(), getSession.getUserId());
//        Assert.assertEquals(open.getTimestamp(), getSession.getTimestamp());
//        Assert.assertEquals(open.getSignature(), getSession.getSignature());
//    }
//
////    @Test
////    @TestCaseName("Run testDeleteSessionByUserId for deleteSessionByUserId({1})")
////    @Category({PositiveImplementation.class, ServiceImplementation.class})
////    @Parameters(
////            source = CaseDataSessionProvider.class,
////            method = "validSessionsCaseData"
////    )
////    public void testDeleteSessionByUserId(
////            @NotNull final User user,
////            @NotNull final Session session
////    ) {
////        Assert.assertNotNull(this.userService);
////        Assert.assertNotNull(this.sessionService);
////        Assert.assertNotNull(user);
////        Assert.assertNotNull(session);
////        @NotNull final User addUser = this.userService.addUser(user.getLogin(), user.getPasswordHash());
////        Assert.assertNotNull(addUser);
////        @NotNull final Session open = this.sessionService.openSession(user.getLogin(), user.getPasswordHash());
////        Assert.assertNotNull(open);
////
////        final boolean deleteSession = this.sessionService.deleteSessionByUserId(open);
////        Assert.assertTrue(deleteSession);
////    }
//
//    @Test
//    @TestCaseName("Run testValidateUserData for verifyValidUserData(\"{0}\", \"{1}\"")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testValidateUserData(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final User validate = this.sessionService.validateUserData(login, password);
//        Assert.assertNotNull(validate);
//        Assert.assertEquals(addUser.getId(), validate.getId());
//        Assert.assertEquals(addUser.getLogin(), validate.getLogin());
//        Assert.assertEquals(addUser.getPasswordHash(), validate.getPasswordHash());
//        Assert.assertEquals(addUser.getRole(), validate.getRole());
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidUserData for verifyValidUserData(\"{0}\", \"{1}\"")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testVerifyValidUserData(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final UserDataValidState userDataValidState = this.sessionService.verifyValidUserData(login, password);
//        Assert.assertNotNull(userDataValidState);
//        Assert.assertEquals(UserDataValidState.SUCCESS, userDataValidState);
//    }
//
//    @Test
//    @TestCaseName("Run testValidateSession for validateSession({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testValidateSession(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        @NotNull final Session open = this.sessionService.persist(signatureSession);
//        Assert.assertNotNull(open);
//
//        @NotNull final Session validate = this.sessionService.validateSession(signatureSession);
//        Assert.assertNotNull(validate);
//        Assert.assertEquals(open.getId(), validate.getId());
//        Assert.assertEquals(open.getUserId(), validate.getUserId());
//        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
//        Assert.assertEquals(open.getSignature(), validate.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidUserData for validateSession({1}, role)")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testValidateSessionWithRole(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
//        Assert.assertNotNull(userRepository);
//        @NotNull final User addUser = userRepository.persist(user);
//        Assert.assertNotNull(addUser);
//        @NotNull final Session open = this.sessionService.persist(signatureSession);
//        Assert.assertNotNull(open);
//
//        @NotNull final Session validate = this.sessionService.validateSession(signatureSession, user.getRole());
//        Assert.assertNotNull(validate);
//        Assert.assertEquals(open.getId(), validate.getId());
//        Assert.assertEquals(open.getUserId(), validate.getUserId());
//        Assert.assertEquals(open.getTimestamp(), validate.getTimestamp());
//        Assert.assertEquals(open.getSignature(), validate.getSignature());
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidSessionState for validateSession({1})")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testVerifyValidSessionState(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final Session signatureSession = this.sessionService.setSignature(session);
//        Assert.assertNotNull(signatureSession);
//        @NotNull final Session open = this.sessionService.persist(signatureSession);
//        Assert.assertNotNull(open);
//
//        @NotNull final SessionValidState verifyValidSessionState = this.sessionService.verifyValidSessionState(open);
//        Assert.assertNotNull(verifyValidSessionState);
//        Assert.assertEquals(SessionValidState.SUCCESS, verifyValidSessionState);
//    }
//
//    @Test
//    @TestCaseName("Run testVerifyValidPermissionState for verifyValidPermission(\"{0}\", \"{1}\")")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDataUserProvider.class,
//            method = "validUsersMainFieldsCaseData"
//    )
//    public void testVerifyValidPermissionState(
//            @NotNull final String login,
//            @NotNull final String password
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.authService);
//        Assert.assertNotNull(this.sessionService);
//        Assert.assertNotNull(login);
//        Assert.assertNotNull(password);
//        @NotNull final User addUser = this.userService.addUser(login, password);
//        Assert.assertNotNull(addUser);
//
//        @NotNull final PermissionValidState permissionValidState =
//                this.authService.verifyValidPermission(addUser.getId(), addUser.getRole());
//        Assert.assertNotNull(permissionValidState);
//        Assert.assertEquals(PermissionValidState.SUCCESS, permissionValidState);
//    }

}