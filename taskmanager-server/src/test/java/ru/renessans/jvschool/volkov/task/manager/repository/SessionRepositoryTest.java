package ru.renessans.jvschool.volkov.task.manager.repository;

import junitparams.JUnitParamsRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;
import ru.renessans.jvschool.volkov.task.manager.service.ConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.service.PropertyService;

@RunWith(value = JUnitParamsRunner.class)
public final class SessionRepositoryTest {


    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService(PROPERTY_SERVICE);

//    @Before
//    public void loadConfigurationBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.entityManagerFactoryService.build();
//    }
//
//    @Test
//    @TestCaseName("Run testContainsUserId for containsUserId({0})")
//    @Category({PositiveImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testContainsUserId(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
//        Assert.assertNotNull(sessionRepository);
//        @NotNull final Session addRecord = sessionRepository.persist(session);
//        Assert.assertNotNull(addRecord);
//
//        final boolean isContainsUser = sessionRepository.containsUserId(user.getId());
//        Assert.assertTrue(isContainsUser);
//    }
//
//    @Test
//    @TestCaseName("Run testGetSessionByUserId for getSessionByUserId({0})")
//    @Category({PositiveImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testGetSessionByUserId(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
//        Assert.assertNotNull(sessionRepository);
//        @NotNull final Session addRecord = sessionRepository.persist(session);
//        Assert.assertNotNull(addRecord);
//
//        @Nullable final Session getSession = sessionRepository.getSessionByUserId(user.getId());
//        Assert.assertNotNull(getSession);
//        Assert.assertEquals(session.getId(), getSession.getId());
//        Assert.assertEquals(session.getUserId(), getSession.getUserId());
//        Assert.assertEquals(session.getTimestamp(), getSession.getTimestamp());
//    }

//    @Test
//    @TestCaseName("Run testDeleteByUserId for deleteByUserId({0})")
//    @Category({PositiveImplementation.class, RepositoryImplementation.class})
//    @Parameters(
//            source = CaseDataSessionProvider.class,
//            method = "validSessionsCaseData"
//    )
//    public void testDeleteByUserId(
//            @NotNull final User user,
//            @NotNull final Session session
//    ) {
//        Assert.assertNotNull(this.entityManagerFactoryService);
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(session);
//        @NotNull final EntityManager entityManager = this.entityManagerFactoryService.getEntityManager();
//        Assert.assertNotNull(entityManager);
//        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
//        Assert.assertNotNull(sessionRepository);
//        @NotNull final Session addRecord = sessionRepository.persist(session);
//        Assert.assertNotNull(addRecord);
//
//        final boolean isDeleteSession = sessionRepository.deleteByUserId(user.getId());
//        Assert.assertTrue(isDeleteSession);
//    }

}