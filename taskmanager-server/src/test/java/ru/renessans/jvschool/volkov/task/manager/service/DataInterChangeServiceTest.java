package ru.renessans.jvschool.volkov.task.manager.service;

import junitparams.JUnitParamsRunner;
import org.jetbrains.annotations.NotNull;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.api.service.IConfigurationService;
import ru.renessans.jvschool.volkov.task.manager.api.service.IPropertyService;

@RunWith(value = JUnitParamsRunner.class)
public final class DataInterChangeServiceTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IConfigurationService CONFIG_SERVICE = new ConfigurationService(PROPERTY_SERVICE);

//    @NotNull
//    private final IUserService userService = new UserService();
//
//    @NotNull
//    private final IProjectUserService projectUserService = new ProjectUserService();
//
//    @NotNull
//    private final ITaskUserService taskService = new TaskUserService(projectUserService);
//
//    @NotNull
//    private final IProjectUserService projectService = new ProjectUserService();
//
//    @NotNull
//    private final IAdapterContextRepository adapterRepository = new AdapterContextRepository();
//
//    @NotNull
//    private final IAdapterContextService adapterService = new AdapterContextService(adapterRepository);
//
//    @NotNull
//    private final IUserUnlimitedAdapterService userAdapter = adapterService.getUserUnlimitedAdapter();
//
//    @NotNull
//    private final ITaskAdapterService taskAdapter = adapterService.getTaskAdapter();
//
//    @NotNull
//    private final IProjectAdapterService projectAdapter = adapterRepository.getProjectAdapter();
//
//    @NotNull
//    private final IDomainService domainService = new DomainService(
//            userService, taskService, projectService, adapterService
//    );
//
//    @NotNull
//    private final IDataInterChangeService dataService = new DataInterChangeService(CONFIG_SERVICE, domainService);

//    @Before
//    public void loadConfigBefore() {
//        Assert.assertNotNull(this.configService);
//        this.configService.load();
//        this.entityManagerFactoryService.build();
//    }
//
//    @After
//    public void clearTempFilesAfter() {
//        Assert.assertNotNull(this.configService);
//        FileUtil.delete(this.configService.getBase64Pathname());
//        FileUtil.delete(this.configService.getBinPathname());
//        FileUtil.delete(this.configService.getBase64Pathname());
//        FileUtil.delete(this.configService.getJsonPathname());
//        FileUtil.delete(this.configService.getXmlPathname());
//        FileUtil.delete(this.configService.getYamlPathname());
//    }
//
//    @Test
//    @TestCaseName("Run testDataBinClear for dataBinClear()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testDataBinClear() {
//        Assert.assertNotNull(this.configService);
//        Assert.assertNotNull(this.dataService);
//        @NotNull final File create = FileUtil.create(this.configService.getBinPathname());
//        Assert.assertNotNull(create);
//        final boolean isBinClear = this.dataService.dataBinClear();
//        Assert.assertTrue(isBinClear);
//    }
//
//    @Test
//    @TestCaseName("Run testDataBase64Clear for dataBase64Clear()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testDataBase64Clear() {
//        Assert.assertNotNull(this.configService);
//        Assert.assertNotNull(this.dataService);
//        @NotNull final File create = FileUtil.create(this.configService.getBase64Pathname());
//        Assert.assertNotNull(create);
//        final boolean isBase64Clear = this.dataService.dataBase64Clear();
//        Assert.assertTrue(isBase64Clear);
//    }
//
//    @Test
//    @TestCaseName("Run testDataJsonClear for dataJsonClear()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testDataJsonClear() {
//        Assert.assertNotNull(this.configService);
//        Assert.assertNotNull(this.dataService);
//        @NotNull final File create = FileUtil.create(this.configService.getJsonPathname());
//        Assert.assertNotNull(create);
//        final boolean isJsonClear = this.dataService.dataJsonClear();
//        Assert.assertTrue(isJsonClear);
//    }
//
//    @Test
//    @TestCaseName("Run testDataXmlClear for dataXmlClear()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testDataXmlClear() {
//        Assert.assertNotNull(this.configService);
//        Assert.assertNotNull(this.dataService);
//        @NotNull final File create = FileUtil.create(this.configService.getXmlPathname());
//        Assert.assertNotNull(create);
//        final boolean isXmlClear = this.dataService.dataXmlClear();
//        Assert.assertTrue(isXmlClear);
//    }
//
//    @Test
//    @TestCaseName("Run testDataYamlClear for dataYamlClear()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    public void testDataYamlClear() {
//        Assert.assertNotNull(this.configService);
//        Assert.assertNotNull(this.dataService);
//        @NotNull final File create = FileUtil.create(this.configService.getYamlPathname());
//        Assert.assertNotNull(create);
//        final boolean isYamlClear = this.dataService.dataYamlClear();
//        Assert.assertTrue(isYamlClear);
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataBin for exportDataBin()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataBin(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//
//        @NotNull final DomainDTO domain = this.dataService.exportDataBin();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(this.userService.getAllRecords().toString(), domain.getUsers().toString());
//        Assert.assertEquals(this.projectService.getAllRecords().toString(), domain.getProjects().toString());
//        Assert.assertEquals(this.taskService.getAllRecords().toString(), domain.getTasks().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataBase64 for exportDataBase64()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataBase64(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//
//        @NotNull final DomainDTO domain = this.dataService.exportDataBase64();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(this.userService.getAllRecords().toString(), domain.getUsers().toString());
//        Assert.assertEquals(this.projectService.getAllRecords().toString(), domain.getProjects().toString());
//        Assert.assertEquals(this.taskService.getAllRecords().toString(), domain.getTasks().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataJson for exportDataJson()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataJson(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//
//        @NotNull final DomainDTO domain = this.dataService.exportDataJson();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(this.userService.getAllRecords().toString(), domain.getUsers().toString());
//        Assert.assertEquals(this.projectService.getAllRecords().toString(), domain.getProjects().toString());
//        Assert.assertEquals(this.taskService.getAllRecords().toString(), domain.getTasks().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataXml for exportDataXml()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataXml(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//
//        @NotNull final DomainDTO domain = this.dataService.exportDataXml();
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(this.userService.getAllRecords().toString(), domain.getUsers().toString());
//        Assert.assertEquals(this.projectService.getAllRecords().toString(), domain.getProjects().toString());
//        Assert.assertEquals(this.taskService.getAllRecords().toString(), domain.getTasks().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testExportDataYaml for exportDataYaml()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testExportDataYaml(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//
//        @NotNull final DomainDTO domain = this.dataService.exportDataYaml();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(this.userService.getAllRecords().toString(), domain.getUsers().toString());
//        Assert.assertEquals(this.projectService.getAllRecords().toString(), domain.getProjects().toString());
//        Assert.assertEquals(this.taskService.getAllRecords().toString(), domain.getTasks().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataBin for importDataBin()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataBin(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final DomainDTO exportDomain = this.dataService.exportDataBin();
//        Assert.assertNotNull(exportDomain);
//
//        @NotNull final DomainDTO domain = this.dataService.importDataBin();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataBase64 for importDataBase64()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataBase64(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final DomainDTO exportDomain = this.dataService.exportDataBase64();
//        Assert.assertNotNull(exportDomain);
//
//        @NotNull final DomainDTO domain = this.dataService.importDataBase64();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataJson for importDataJson()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataJson(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final DomainDTO exportDomain = this.dataService.exportDataJson();
//        Assert.assertNotNull(exportDomain);
//
//        @NotNull final DomainDTO domain = this.dataService.importDataJson();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataXml for importDataXml()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataXml(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final DomainDTO exportDomain = this.dataService.exportDataXml();
//        Assert.assertNotNull(exportDomain);
//
//        @NotNull final DomainDTO domain = this.dataService.importDataXml();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }
//
//    @Test
//    @TestCaseName("Run testImportDataYaml for importDataYaml()")
//    @Category({PositiveImplementation.class, ServiceImplementation.class})
//    @Parameters(
//            source = CaseDomainProvider.class,
//            method = "validCollectionDomainsCaseData"
//    )
//    public void testImportDataYaml(
//            @NotNull final DomainDTO data
//    ) {
//        Assert.assertNotNull(this.userService);
//        Assert.assertNotNull(this.adapterRepository);
//        Assert.assertNotNull(this.adapterService);
//        Assert.assertNotNull(this.userAdapter);
//        Assert.assertNotNull(this.projectAdapter);
//        Assert.assertNotNull(this.taskAdapter);
//        Assert.assertNotNull(this.taskService);
//        Assert.assertNotNull(this.projectService);
//        Assert.assertNotNull(this.dataService);
//        Assert.assertNotNull(data);
//        @NotNull final Collection<User> conversionUsers =
//                data.getUsers().stream().map(this.userAdapter::toModel).collect(Collectors.toList());
//        Assert.assertNotNull(conversionUsers);
//        @NotNull final Collection<User> setAllUserRecords = this.userService.setAllRecords(conversionUsers);
//        Assert.assertNotNull(setAllUserRecords);
//        @NotNull final Collection<Task> conversionTasks =
//                data.getTasks().stream().map(this.taskAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Task> setAllTaskRecords = this.taskService.setAllRecords(conversionTasks);
//        Assert.assertNotNull(setAllTaskRecords);
//        @NotNull final Collection<Project> conversionProjects =
//                data.getProjects().stream().map(this.projectAdapter::toModel).collect(Collectors.toList());
//        @NotNull final Collection<Project> setAllProjectRecords = this.projectService.setAllRecords(conversionProjects);
//        Assert.assertNotNull(setAllProjectRecords);
//        @NotNull final DomainDTO exportDomain = this.dataService.exportDataYaml();
//        Assert.assertNotNull(exportDomain);
//
//        @NotNull final DomainDTO domain = this.dataService.importDataYaml();
//        Assert.assertNotNull(domain);
//        Assert.assertNotEquals(0, domain.getUsers().size());
//        Assert.assertNotEquals(0, domain.getTasks().size());
//        Assert.assertNotEquals(0, domain.getProjects().size());
//        Assert.assertEquals(domain.getUsers().toString(), this.userService.getAllRecords().toString());
//        Assert.assertEquals(domain.getProjects().toString(), this.projectService.getAllRecords().toString());
//        Assert.assertEquals(domain.getTasks().toString(), this.taskService.getAllRecords().toString());
//    }

}