package ru.renessans.jvschool.volkov.task.manager.api.controller.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

public interface ITaskController {

    @NotNull
    @GetMapping("/tasks")
    ModelAndView index();

    @NotNull
    @GetMapping("/task/create")
    ModelAndView create();

    @NotNull
    @PostMapping("/task/create")
    ModelAndView create(
            @ModelAttribute("task") @NotNull TaskDTO taskDTO,
            @NotNull BindingResult result
    );

    @NotNull
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    ModelAndView view(
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @GetMapping("/task/delete/{id}")
    ModelAndView delete(
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @SneakyThrows
    @GetMapping("/task/edit/{id}")
    ModelAndView edit(
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @PostMapping("/task/edit/{id}")
    ModelAndView edit(
            @ModelAttribute("task") @NotNull TaskDTO taskDTO,
            @NotNull BindingResult result
    );

}