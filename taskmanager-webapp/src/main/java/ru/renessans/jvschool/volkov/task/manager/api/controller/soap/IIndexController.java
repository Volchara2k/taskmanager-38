package ru.renessans.jvschool.volkov.task.manager.api.controller.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;

public interface IIndexController {

    @NotNull
    @GetMapping("/")
    String index();

}