package ru.renessans.jvschool.volkov.task.manager.controller.soap;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.api.controller.soap.ITaskController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.Status;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Task;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class TaskController implements ITaskController {

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping("/tasks")
    @Override
    public ModelAndView index() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-list");
        @NotNull final Collection<Task> tasks = this.taskUserService.exportOwnerUser();
        modelAndView.addObject("tasks", tasks);
        return modelAndView;
    }

    @NotNull
    @GetMapping("/task/create")
    @Override
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-settable");
        modelAndView.addObject("task", new TaskDTO());
        modelAndView.addObject("statuses", Status.values());
        @NotNull final Collection<ProjectDTO> projects = this.projectUserService.exportOwnerUser()
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @NotNull
    @PostMapping("/task/create")
    @Override
    public ModelAndView create(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        this.taskUserService.addOwnerUser(task);
        return new ModelAndView("redirect:/tasks");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    @Override
    public ModelAndView view(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.taskUserService.getRecordById(id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-view");
        modelAndView.addObject("task", task);
        return modelAndView;
    }

    @Override
    @NotNull
    @GetMapping("/task/delete/{id}")
    public ModelAndView delete(
            @PathVariable("id") @NotNull final String id
    ) {
        this.taskUserService.deleteRecordById(id);
        return new ModelAndView("redirect:/tasks");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/task/edit/{id}")
    @Override
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.taskUserService.getRecordById(id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        @Nullable final TaskDTO taskDTO = this.taskAdapterService.toDTO(task);
        if (Objects.isNull(taskDTO)) throw new InvalidOwnerUserException();

        @NotNull final ModelAndView modelAndView = new ModelAndView("task/task-settable");
        modelAndView.addObject("task", taskDTO);
        modelAndView.addObject("statuses", Status.values());
        @NotNull final Collection<ProjectDTO> projects = this.projectUserService.exportOwnerUser()
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
        modelAndView.addObject("projects", projects);

        return modelAndView;
    }

    @NotNull
    @SneakyThrows
    @PostMapping("/task/edit/{id}")
    @Override
    public ModelAndView edit(
            @ModelAttribute("task") @NotNull final TaskDTO taskDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        final boolean emptyId = ValidRuleUtil.isNullOrEmpty(task.getId());
        if (emptyId) throw new InvalidIdException();
        this.taskUserService.addOwnerUser(task);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/task/view/{id}");
        modelAndView.addObject("id", task.getId());
        return modelAndView;
    }

}