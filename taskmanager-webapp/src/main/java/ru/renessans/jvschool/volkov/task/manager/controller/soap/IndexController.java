package ru.renessans.jvschool.volkov.task.manager.controller.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import ru.renessans.jvschool.volkov.task.manager.api.controller.soap.IIndexController;

@Controller
public class IndexController implements IIndexController {

    @NotNull
    @GetMapping("/")
    @Override
    public String index() {
        return "index";
    }

}