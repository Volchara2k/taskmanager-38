package ru.renessans.jvschool.volkov.task.manager.controller.soap;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.api.controller.soap.IProjectController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.enumeration.Status;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

@Controller
@RequiredArgsConstructor
public class ProjectController implements IProjectController {

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping("/projects")
    @Override
    public ModelAndView index() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-list");
        @NotNull final Collection<Project> projects = this.projectUserService.exportOwnerUser();
        modelAndView.addObject("projects", projects);
        return modelAndView;
    }

    @NotNull
    @GetMapping("/project/create")
    @Override
    public ModelAndView create() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-settable");
        modelAndView.addObject("project", new ProjectDTO());
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @NotNull
    @PostMapping("/project/create")
    @Override
    public ModelAndView create(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        this.projectUserService.addOwnerUser(project);
        return new ModelAndView("redirect:/projects");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @Override
    public ModelAndView view(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.projectUserService.getRecordById(id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-view");
        modelAndView.addObject("project", project);
        return modelAndView;
    }

    @NotNull
    @GetMapping("/project/delete/{id}")
    @Override
    public ModelAndView delete(
            @PathVariable("id") @NotNull final String id
    ) {
        this.projectUserService.cascadeDeleteRecordById(id);
        return new ModelAndView("redirect:/projects");
    }

    @NotNull
    @SneakyThrows
    @GetMapping("/project/edit/{id}")
    @Override
    public ModelAndView edit(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.projectUserService.getRecordById(id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        @Nullable final ProjectDTO projectDTO = this.projectAdapterService.toDTO(project);
        if (Objects.isNull(projectDTO)) throw new InvalidOwnerUserException();
        @NotNull final ModelAndView modelAndView = new ModelAndView("project/project-settable");
        modelAndView.addObject("project", projectDTO);
        return modelAndView;
    }

    @NotNull
    @SneakyThrows
    @PostMapping("/project/edit/{id}")
    @Override
    public ModelAndView edit(
            @ModelAttribute("project") @NotNull final ProjectDTO projectDTO,
            @NotNull final BindingResult result
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        final boolean emptyId = ValidRuleUtil.isNullOrEmpty(project.getId());
        if (emptyId) throw new InvalidIdException();
        this.projectUserService.addOwnerUser(project);
        @NotNull final ModelAndView modelAndView = new ModelAndView("redirect:/project/view/{id}");
        modelAndView.addObject("id", project.getId());
        return modelAndView;
    }

}