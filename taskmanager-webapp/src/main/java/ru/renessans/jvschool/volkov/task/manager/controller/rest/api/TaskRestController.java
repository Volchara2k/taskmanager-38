package ru.renessans.jvschool.volkov.task.manager.controller.rest.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.controller.rest.ITaskRestController;
import ru.renessans.jvschool.volkov.task.manager.api.service.ITaskUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.ITaskAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Task;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/tasks",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class TaskRestController implements ITaskRestController {

    @NotNull
    private final ITaskUserService taskUserService;

    @NotNull
    private final ITaskAdapterService taskAdapterService;

    @NotNull
    @GetMapping
    @ApiOperation(value = "Get all tasks")
    @Override
    public Collection<TaskDTO> getAllTasks() {
        return this.taskUserService.exportOwnerUser()
                .stream()
                .map(this.taskAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    @ApiOperation(value = "Get task by id")
    @Override
    public TaskDTO getTaskById(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Task task = this.taskUserService.getRecordById(id);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        return this.taskAdapterService.toDTO(task);
    }

    @Nullable
    @RequestMapping(value = "/task/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(value = "Create task",
            notes = "Field required"
    )
    @Override
    public TaskDTO createTask(
            @ApiParam(required = true) @RequestBody @NotNull final TaskDTO taskDTO
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        try {
            this.taskUserService.addOwnerUser(task);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return taskDTO;
    }

    @DeleteMapping("/task/{id}")
    @ApiOperation(value = "Delete task by id")
    @Override
    public int deleteTaskById(
            @PathVariable("id") @NotNull final String id
    ) {
        return this.taskUserService.cascadeDeleteRecordById(id);
    }

    @Nullable
    @SneakyThrows
    @RequestMapping(value = "/task/edit/{id}",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(value = "Edit task",
            notes = "Field required"
    )
    @Override
    public TaskDTO editTask(
            @ApiParam(required = true) @RequestBody @NotNull final TaskDTO taskDTO
    ) {
        @Nullable final Task task = this.taskAdapterService.toModel(taskDTO);
        if (Objects.isNull(task)) throw new InvalidOwnerUserException();
        try {
            this.taskUserService.addOwnerUser(task);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return taskDTO;
    }

}