package ru.renessans.jvschool.volkov.task.manager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.renessans.jvschool.volkov.task.manager.api.service.IOwnerUserService;
import ru.renessans.jvschool.volkov.task.manager.exception.illegal.IllegalIndexException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidDescriptionException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidIdException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidTitleException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.service.InvalidValueException;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.user.InvalidUserIdException;
import ru.renessans.jvschool.volkov.task.manager.model.AbstractUserOwner;
import ru.renessans.jvschool.volkov.task.manager.repository.IOwnerUserRepository;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Transactional
public abstract class AbstractUserOwnerService<E extends AbstractUserOwner> extends AbstractService<E> implements IOwnerUserService<E> {

    private final IOwnerUserRepository<E> ownerUserRepository;

    protected AbstractUserOwnerService(
            @NotNull final IOwnerUserRepository<E> ownerUserRepository
    ) {
        super(ownerUserRepository);
        this.ownerUserRepository = ownerUserRepository;
    }

    @NotNull
    @SneakyThrows
    @Override
    public E addOwnerUser(
            @Nullable E value
    ) {
        if (Objects.isNull(value)) throw new InvalidValueException();
        return super.save(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateOwnerUserByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        if (ValidRuleUtil.isNullOrEmpty(newTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = this.getOwnerUserByIndex(userId, index);
        if (Objects.isNull(value)) throw new InvalidOwnerUserException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.save(value);
    }

    @NotNull
    @SneakyThrows
    @Override
    public E updateOwnerUserById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String newTitle,
            @Nullable final String newDescription
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        if (ValidRuleUtil.isNullOrEmpty(newTitle)) throw new InvalidTitleException();
        if (ValidRuleUtil.isNullOrEmpty(newDescription)) throw new InvalidDescriptionException();

        @Nullable final E value = this.getOwnerUserById(userId, id);
        if (Objects.isNull(value)) throw new InvalidOwnerUserException();
        value.setTitle(newTitle);
        value.setDescription(newDescription);

        return super.save(value);
    }

    @SneakyThrows
    @Override
    public int deleteOwnerUserById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        @Nullable final E value = this.getOwnerUserById(userId, id);
        if (Objects.isNull(value)) return 0;
        return super.cascadeDeleteRecordById(value.getId());
    }

    @SneakyThrows
    @Override
    public int deleteOwnerUserByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        @Nullable final E value = this.getOwnerUserByTitle(userId, title);
        if (Objects.isNull(value)) return 0;
        return super.cascadeDeleteRecordById(value.getId());
    }

    @SneakyThrows
    @Override
    public int deleteOwnerUserAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        @NotNull final Collection<E> values = this.getOwnerUserAll(userId);
        values.forEach(this.ownerUserRepository::delete);
        final boolean notDeleted = this.getOwnerUserAll(userId).size() != 0;
        if (notDeleted) return 0;
        return 1;
    }

    @SneakyThrows
    @Override
    public int deleteOwnerUserByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        @NotNull final List<E> values = this.getOwnerUserAll(userId);
        if (index >= values.size()) return 0;
        @Nullable final E value = values.get(index);
        if (Objects.isNull(value)) return 0;
        return super.cascadeDeleteRecordById(value.getId());
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getOwnerUserByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(index)) throw new IllegalIndexException();
        @NotNull final List<E> values = this.getOwnerUserAll(userId);
        if (index >= values.size()) return null;
        return values.get(index);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getOwnerUserById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidIdException();
        return this.ownerUserRepository.getOwnerUserById(userId, id);
    }

    @Nullable
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public E getOwnerUserByTitle(
            @Nullable final String userId,
            @Nullable final String title
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        if (ValidRuleUtil.isNullOrEmpty(title)) throw new InvalidTitleException();
        return this.ownerUserRepository.getOwnerUserByTitle(userId, title);
    }

    @NotNull
    @Transactional(readOnly = true)
    @SneakyThrows
    @Override
    public List<E> getOwnerUserAll(
            @Nullable final String userId
    ) {
        if (ValidRuleUtil.isNullOrEmpty(userId)) throw new InvalidUserIdException();
        return this.ownerUserRepository.getAllOwnerUser(userId);
    }

    @NotNull
    @Transactional(readOnly = true)
    @Override
    public List<E> exportOwnerUser() {
        return this.ownerUserRepository.exportOwnerUser();
    }

}