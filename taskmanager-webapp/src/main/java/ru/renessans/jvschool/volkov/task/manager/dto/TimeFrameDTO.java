package ru.renessans.jvschool.volkov.task.manager.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.Date;

@Data
@XmlType
@SuperBuilder
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class TimeFrameDTO implements Serializable {

    private static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd";

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = DATE_FORMAT_PATTERN
    )
    @DateTimeFormat(pattern = DATE_FORMAT_PATTERN)
    private Date creationDate = new Date(System.currentTimeMillis());

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = DATE_FORMAT_PATTERN
    )
    @DateTimeFormat(pattern = DATE_FORMAT_PATTERN)
    private Date startDate;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING,
            pattern = DATE_FORMAT_PATTERN
    )
    @DateTimeFormat(pattern = DATE_FORMAT_PATTERN)
    private Date endDate;

}