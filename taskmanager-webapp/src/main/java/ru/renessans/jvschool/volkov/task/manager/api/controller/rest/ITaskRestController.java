package ru.renessans.jvschool.volkov.task.manager.api.controller.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.dto.TaskDTO;

import java.util.Collection;

public interface ITaskRestController {
    @NotNull
    @GetMapping
    @ApiOperation(value = "Get all tasks")
    Collection<TaskDTO> getAllTasks();

    @Nullable
    @SneakyThrows
    @GetMapping("/task/view/{id}")
    @ApiOperation(value = "Get task by id")
    TaskDTO getTaskById(
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @RequestMapping(value = "/task/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(value = "Create task",
            notes = "Field required"
    )
    TaskDTO createTask(
            @ApiParam(required = true) @RequestBody @NotNull TaskDTO taskDTO
    );

    @DeleteMapping("/task/{id}")
    @ApiOperation(value = "Delete task by id")
    int deleteTaskById(
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @SneakyThrows
    @RequestMapping(value = "/task/edit/{id}",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(value = "Edit task",
            notes = "Field required"
    )
    TaskDTO editTask(
            @ApiParam(required = true) @RequestBody @NotNull TaskDTO taskDTO
    );
}
