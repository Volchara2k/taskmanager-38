package ru.renessans.jvschool.volkov.task.manager.api.controller.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;

public interface IProjectController {

    @NotNull
    @GetMapping("/projects")
    ModelAndView index();

    @NotNull
    @GetMapping("/project/create")
    ModelAndView create();

    @NotNull
    @PostMapping("/project/create")
    ModelAndView create(
            @ModelAttribute("project") @NotNull ProjectDTO projectDTO,
            @NotNull BindingResult result
    );

    @NotNull
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    ModelAndView view(
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @GetMapping("/project/delete/{id}")
    ModelAndView delete(
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @SneakyThrows
    @GetMapping("/project/edit/{id}")
    ModelAndView edit(
            @PathVariable("id") @NotNull String id
    );

    @NotNull
    @PostMapping("/project/edit/{id}")
    ModelAndView edit(
            @ModelAttribute("project") @NotNull ProjectDTO projectDTO,
            @NotNull BindingResult result
    );

}