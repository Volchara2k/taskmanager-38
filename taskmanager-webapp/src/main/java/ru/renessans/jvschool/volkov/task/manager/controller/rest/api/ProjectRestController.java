package ru.renessans.jvschool.volkov.task.manager.controller.rest.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.api.controller.rest.IProjectRestController;
import ru.renessans.jvschool.volkov.task.manager.api.service.IProjectUserService;
import ru.renessans.jvschool.volkov.task.manager.api.service.adapter.IProjectAdapterService;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.exception.invalid.owner.InvalidOwnerUserException;
import ru.renessans.jvschool.volkov.task.manager.model.Project;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/projects",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public class ProjectRestController implements IProjectRestController {

    @NotNull
    private final IProjectUserService projectUserService;

    @NotNull
    private final IProjectAdapterService projectAdapterService;

    @NotNull
    @GetMapping
    @ApiOperation(value = "Get all projects")
    @Override
    public Collection<ProjectDTO> getAllProjects() {
        return this.projectUserService.exportOwnerUser()
                .stream()
                .map(this.projectAdapterService::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @ApiOperation(value = "Get project by id")
    @Override
    public ProjectDTO getProjectById(
            @PathVariable("id") @NotNull final String id
    ) {
        @Nullable final Project project = this.projectUserService.getRecordById(id);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        return this.projectAdapterService.toDTO(project);
    }

    @Nullable
    @RequestMapping(value = "/project/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(value = "Create project",
            notes = "Field required"
    )
    @Override
    public ProjectDTO createProject(
            @ApiParam(required = true) @RequestBody @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        try {
            this.projectUserService.addOwnerUser(project);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return projectDTO;
    }

    @DeleteMapping("/project/{id}")
    @ApiOperation(value = "Delete project by id")
    @Override
    public int deleteProjectById(
            @PathVariable("id") @NotNull final String id
    ) {
        return this.projectUserService.cascadeDeleteRecordById(id);
    }

    @Nullable
    @SneakyThrows
    @RequestMapping(value = "/project/edit/{id}",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(value = "Edit project",
            notes = "Field required"
    )
    @Override
    public ProjectDTO editProject(
            @ApiParam(required = true) @RequestBody @NotNull final ProjectDTO projectDTO
    ) {
        @Nullable final Project project = this.projectAdapterService.toModel(projectDTO);
        if (Objects.isNull(project)) throw new InvalidOwnerUserException();
        try {
            this.projectUserService.addOwnerUser(project);
        } catch (@NotNull final Exception exception) {
            return null;
        }
        return projectDTO;
    }

}