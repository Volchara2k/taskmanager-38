package ru.renessans.jvschool.volkov.task.manager.api.controller.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.renessans.jvschool.volkov.task.manager.dto.ProjectDTO;

import java.util.Collection;

public interface IProjectRestController {

    @NotNull
    @GetMapping
    @ApiOperation(value = "Get all projects")
    Collection<ProjectDTO> getAllProjects();

    @Nullable
    @SneakyThrows
    @GetMapping("/project/view/{id}")
    @ApiOperation(value = "Get project by id")
    ProjectDTO getProjectById(
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @RequestMapping(value = "/project/create",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(value = "Create project",
            notes = "Field required"
    )
    ProjectDTO createProject(
            @ApiParam(required = true) @RequestBody @NotNull ProjectDTO projectDTO
    );

    @DeleteMapping("/project/{id}")
    @ApiOperation(value = "Delete project by id")
    int deleteProjectById(
            @PathVariable("id") @NotNull String id
    );

    @Nullable
    @SneakyThrows
    @RequestMapping(value = "/project/edit/{id}",
            method = {RequestMethod.POST, RequestMethod.PUT}
    )
    @ApiOperation(value = "Edit project",
            notes = "Field required"
    )
    ProjectDTO editProject(
            @ApiParam(required = true) @RequestBody @NotNull ProjectDTO projectDTO
    );

}