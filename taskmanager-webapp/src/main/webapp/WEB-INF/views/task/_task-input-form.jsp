<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="task_write-content" align="center">

    <div><p>Project:</p></div>
    <div>
        <form:select path="projectId">
            <form:option value="${null}" label="NONE" />
            <form:options items="${projects}" itemLabel="title" itemValue="id" />
        </form:select>
    </div>

    <jsp:include page="../../include/_input-form.jsp" />

</div>