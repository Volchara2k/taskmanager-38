<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../../include/_header.jsp" />

<c:choose>
    <c:when test="${not empty task.title}">
        <div class="task_activity-content">
            <h2>Edit task: &laquo;${task.title}&raquo;</h2>
        </div>

        <form:form action="/task/edit/${task.id}" method="POST" modelAttribute="task">
            <jsp:include page="_task-input-form.jsp" />
        </form:form>
    </c:when>

    <c:otherwise>
            <div class="task_activity-content">
                <h2>Create task</h2>
            </div>

            <form:form action="/task/create/" method="POST" modelAttribute="task">
                <jsp:include page="_task-input-form.jsp" />
            </form:form>
    </c:otherwise>

</c:choose>

<jsp:include page="../../include/_footer.jsp" />``