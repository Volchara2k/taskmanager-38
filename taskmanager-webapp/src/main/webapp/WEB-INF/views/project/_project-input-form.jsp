<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:input type="hidden" path="id" />
<form:input type="hidden" path="userId" />

<div class="project_write-content" align="center">

    <jsp:include page="../../include/_input-form.jsp" />

</div>