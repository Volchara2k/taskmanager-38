package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sessionValidState.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="sessionValidState"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SUCCESS"/&gt;
 *     &lt;enumeration value="NO_SESSION"/&gt;
 *     &lt;enumeration value="NO_USER_ID"/&gt;
 *     &lt;enumeration value="NO_TIMESTAMP"/&gt;
 *     &lt;enumeration value="NO_SIGNATURE"/&gt;
 *     &lt;enumeration value="DIFFERENT_SIGNATURES"/&gt;
 *     &lt;enumeration value="SESSION_CLOSED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 */
@XmlType(name = "sessionValidState")
@XmlEnum
public enum SessionValidState {

    SUCCESS,
    NO_SESSION,
    NO_USER_ID,
    NO_TIMESTAMP,
    NO_SIGNATURE,
    DIFFERENT_SIGNATURES,
    SESSION_CLOSED;

    public static SessionValidState fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
