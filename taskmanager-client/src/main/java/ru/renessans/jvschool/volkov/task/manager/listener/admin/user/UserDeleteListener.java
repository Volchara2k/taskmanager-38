package ru.renessans.jvschool.volkov.task.manager.listener.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.AbstractAdminListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class UserDeleteListener extends AbstractAdminListener {

    @NotNull
    private static final String CMD_USER_DELETE = "user-delete";

    @NotNull
    private static final String DESC_USER_DELETE = "удалить пользователя (администратор)";

    @NotNull
    private static final String NOTIFY_USER_DELETE =
            "Происходит попытка инициализации удаления пользователя системы. \n" +
                    "Для удаления пользователя введите его логин. ";

    public UserDeleteListener(
            @NotNull final AdminEndpoint adminEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_USER_DELETE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_USER_DELETE;
    }

    @Async
    @Override
    @EventListener(condition = "@userDeleteListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_USER_DELETE);
        @NotNull final String login = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        final int deleteFlag = super.adminEndpoint.deleteUserByLogin(current, login);
        ViewUtil.print(deleteFlag);
    }

}