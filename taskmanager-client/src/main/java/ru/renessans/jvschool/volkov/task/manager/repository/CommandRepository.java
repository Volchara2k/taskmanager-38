package ru.renessans.jvschool.volkov.task.manager.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Collection;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Collection<AbstractListener> listeners;

    @NotNull
    @Override
    public Collection<String> getAllCommands() {
        return this.listeners
                .stream()
                .map(AbstractListener::toString)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<String> getAllTerminalCommands() {
        return this.listeners
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.command()))
                .map(AbstractListener::toString)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<String> getAllArgumentCommands() {
        return this.listeners
                .stream()
                .filter(command -> ValidRuleUtil.isNotNullOrEmpty(command.argument()))
                .map(AbstractListener::toString)
                .collect(Collectors.toList());
    }

}