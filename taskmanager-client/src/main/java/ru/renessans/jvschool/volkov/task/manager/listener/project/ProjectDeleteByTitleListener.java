package ru.renessans.jvschool.volkov.task.manager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ProjectDeleteByTitleListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_DELETE_BY_TITLE = "project-delete-by-title";

    @NotNull
    private static final String DESC_PROJECT_DELETE_BY_TITLE = "удалить проект по заголовку";

    @NotNull
    private static final String NOTIFY_PROJECT_DELETE_BY_TITLE =
            "Происходит попытка инициализации удаления проекта. \n" +
                    "Для удаления проекта по имени введите заголовок проекта из списка. ";

    public ProjectDeleteByTitleListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_DELETE_BY_TITLE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_DELETE_BY_TITLE;
    }

    @Async
    @Override
    @EventListener(condition = "@projectDeleteByTitleListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_DELETE_BY_TITLE);
        @NotNull final String title = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        final int deleteFlag = super.projectEndpoint.deleteProjectByTitle(current, title);
        ViewUtil.print(deleteFlag);
    }

}