package ru.renessans.jvschool.volkov.task.manager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class PasswordUpdateListener extends AbstractUserListener {

    @NotNull
    private static final String CMD_UPDATE_PASSWORD = "update-password";

    @NotNull
    private static final String ARG_UPDATE_PASSWORD = "обновить пароль пользователя";

    @NotNull
    private static final String NOTIFY_UPDATE_PASSWORD =
            "Происходит попытка инициализации смены пароля. \n" +
                    "Для смены пароля введите новый пароль. ";

    public PasswordUpdateListener(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(userEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_UPDATE_PASSWORD;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return ARG_UPDATE_PASSWORD;
    }

    @Async
    @Override
    @EventListener(condition = "@passwordUpdateListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_UPDATE_PASSWORD);
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final String password = ViewUtil.getLine();
        @Nullable final UserLimitedDTO update = super.userEndpoint.updatePassword(current, password);
        ViewUtil.print(update);
    }

}