package ru.renessans.jvschool.volkov.task.manager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.UserLimitedDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ProfileEditListener extends AbstractUserListener {

    @NotNull
    private static final String CMD_EDIT_PROFILE = "edit-profile";

    @NotNull
    private static final String DESC_EDIT_PROFILE = "изменить данные пользователя";

    @NotNull
    private static final String NOTIFY_EDIT_PROFILE =
            "Происходит попытка инициализации редактирования данных пользователя. \n" +
                    "Для обновления данных пользователя введите его имя или имя с фамилией. ";

    public ProfileEditListener(
            @NotNull final UserEndpoint userEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(userEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_EDIT_PROFILE;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_EDIT_PROFILE;
    }

    @Async
    @Override
    @EventListener(condition = "@profileEditListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_EDIT_PROFILE);
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final String firstName = ViewUtil.getLine();
        @Nullable final UserLimitedDTO edit = super.userEndpoint.editProfile(current, firstName);
        ViewUtil.print(edit);
    }

}