package ru.renessans.jvschool.volkov.task.manager.listener.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionEndpoint;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class UserLogOutListener extends AbstractSecurityListener {

    @NotNull
    private static final String CMD_LOG_OUT = "log-out";

    @Nullable
    private static final String DESC_LOG_OUT = "выйти из системы";

    @NotNull
    private static final String NOTIFY_LOG_OUT = "Производится выход пользователя из системы...";

    public UserLogOutListener(
            @NotNull final SessionEndpoint sessionEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(sessionEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_LOG_OUT;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_LOG_OUT;
    }

    @Async
    @Override
    @EventListener(condition = "@userLogOutListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_LOG_OUT);
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @Nullable final SessionDTO close = super.sessionEndpoint.closeSession(current);
        @NotNull final SessionDTO unsubscribe = super.currentSessionService.unsubscribe();
        ViewUtil.print(unsubscribe);
    }

}