package ru.renessans.jvschool.volkov.task.manager.listener.admin;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.listener.AbstractListener;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminEndpoint;

@RequiredArgsConstructor
public abstract class AbstractAdminListener extends AbstractListener {

    @NotNull
    protected final AdminEndpoint adminEndpoint;

    @NotNull
    protected final ICurrentSessionService currentSessionService;

}