package ru.renessans.jvschool.volkov.task.manager.listener.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.AdminDataInterChangeEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.DomainDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.listener.admin.data.AbstractAdminDataListener;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class DataBase64ImportListener extends AbstractAdminDataListener {

    @NotNull
    private static final String CMD_BASE64_IMPORT = "data-base64-import";

    @NotNull
    private static final String DESC_BASE64_IMPORT = "импортировать домен из base64 вида";

    @NotNull
    private static final String NOTIFY_BASE64_IMPORT = "Происходит процесс загрузки домена из base64 вида...";

    public DataBase64ImportListener(
            @NotNull final AdminDataInterChangeEndpoint adminDataInterChangeEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(adminDataInterChangeEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_BASE64_IMPORT;
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public @NotNull String description() {
        return DESC_BASE64_IMPORT;
    }

    @Async
    @Override
    @EventListener(condition = "@dataBase64ImportListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @NotNull final DomainDTO importData = super.adminDataInterChangeEndpoint.importDataBase64(current);
        ViewUtil.print(NOTIFY_BASE64_IMPORT);
        ViewUtil.print(importData.getUsers());
        ViewUtil.print(importData.getTasks());
        ViewUtil.print(importData.getProjects());
    }

}