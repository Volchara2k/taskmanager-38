package ru.renessans.jvschool.volkov.task.manager.bootstrap;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ScannerUtil;
import ru.renessans.jvschool.volkov.task.manager.util.ValidRuleUtil;

import java.util.Objects;

@Component
@RequiredArgsConstructor
public final class Bootstrap {

    @NotNull
    private final ApplicationEventPublisher eventPublisher;

    public void run(@Nullable final String... arguments) {
        final boolean isEmptyArgs = ValidRuleUtil.isNullOrEmpty(arguments);
        if (isEmptyArgs) terminalCommandExecuteLoop();
        else argumentExecute(Objects.requireNonNull(arguments[0]));
    }

    @SuppressWarnings("InfiniteLoopStatement")
    private void terminalCommandExecuteLoop() {
        @NotNull String commandLine;
        while (true) {
            try {
                commandLine = ScannerUtil.getLine();
                this.eventPublisher.publishEvent(new TerminalEvent(commandLine));
            } catch (@NotNull final Exception exception) {
                System.err.print(exception.getMessage() + "\n");
            }
        }
    }

    private void argumentExecute(@NotNull final String argument) {
        try {
            this.eventPublisher.publishEvent(new TerminalEvent(argument));
        } catch (@NotNull final Exception exception) {
            System.err.print(exception.getMessage() + "\n");
        }
    }

}