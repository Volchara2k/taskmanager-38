package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addTaskResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="addTaskResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="task" type="{http://endpoint.manager.task.volkov.jvschool.renessans.ru/}taskDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addTaskResponse", propOrder = {
        "task"
})
public class AddTaskResponse {

    protected TaskDTO task;

    /**
     * Gets the value of the task property.
     *
     * @return possible object is
     * {@link TaskDTO }
     */
    public TaskDTO getTask() {
        return task;
    }

    /**
     * Sets the value of the task property.
     *
     * @param value allowed object is
     *              {@link TaskDTO }
     */
    public void setTask(TaskDTO value) {
        this.task = value;
    }

}
