package ru.renessans.jvschool.volkov.task.manager.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.renessans.jvschool.volkov.task.manager.api.service.ICurrentSessionService;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectDTO;
import ru.renessans.jvschool.volkov.task.manager.endpoint.ProjectEndpoint;
import ru.renessans.jvschool.volkov.task.manager.endpoint.SessionDTO;
import ru.renessans.jvschool.volkov.task.manager.event.TerminalEvent;
import ru.renessans.jvschool.volkov.task.manager.util.ViewUtil;

@Component
public class ProjectViewByIdListener extends AbstractProjectListener {

    @NotNull
    private static final String CMD_PROJECT_VIEW_BY_ID = "project-view-by-id";

    @NotNull
    private static final String DESC_PROJECT_VIEW_BY_ID = "просмотреть проект по идентификатору";

    @NotNull
    private static final String NOTIFY_PROJECT_VIEW_BY_ID =
            "Происходит попытка инициализации отображения проекта. \n" +
                    "Для отображения проекта по идентификатору введите идентификатор проекта из списка. ";

    public ProjectViewByIdListener(
            @NotNull final ProjectEndpoint projectEndpoint,
            @NotNull final ICurrentSessionService currentSessionService
    ) {
        super(projectEndpoint, currentSessionService);
    }

    @NotNull
    @Override
    public String command() {
        return CMD_PROJECT_VIEW_BY_ID;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESC_PROJECT_VIEW_BY_ID;
    }

    @Async
    @Override
    @EventListener(condition = "@projectViewByIdListener.command() == #terminalEvent.inputLine")
    public void handler(@NotNull final TerminalEvent terminalEvent) {
        ViewUtil.print(NOTIFY_PROJECT_VIEW_BY_ID);
        @NotNull final String id = ViewUtil.getLine();
        @Nullable final SessionDTO current = super.currentSessionService.getCurrentSession();
        @Nullable final ProjectDTO view = super.projectEndpoint.getProjectById(current, id);
        ViewUtil.print(view);
    }

}