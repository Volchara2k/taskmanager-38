package ru.renessans.jvschool.volkov.task.manager.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for serverDataResponse complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="serverDataResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="serverData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "serverDataResponse", propOrder = {
        "serverData"
})
public class ServerDataResponse {

    protected String serverData;

    /**
     * Gets the value of the serverData property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getServerData() {
        return serverData;
    }

    /**
     * Sets the value of the serverData property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setServerData(String value) {
        this.serverData = value;
    }

}
