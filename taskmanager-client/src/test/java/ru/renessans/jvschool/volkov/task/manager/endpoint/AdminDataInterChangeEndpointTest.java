package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class AdminDataInterChangeEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminDataInterChangeEndpointService adminDataInterChangeEndpointService = new AdminDataInterChangeEndpointService();

    @NotNull
    private final AdminDataInterChangeEndpoint dataEndpoint = adminDataInterChangeEndpointService.getAdminDataInterChangeEndpointPort();

    @Before
    @SneakyThrows
    public void createFilesBefore() {
        Files.deleteIfExists(Paths.get(DemoDataConst.BIN_LOCATE));
        Files.createFile(new File(DemoDataConst.BIN_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.BASE64_LOCATE));
        Files.createFile(new File(DemoDataConst.BASE64_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.JSON_LOCATE));
        Files.createFile(new File(DemoDataConst.JSON_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.XML_LOCATE));
        Files.createFile(new File(DemoDataConst.XML_LOCATE).toPath());
        Files.deleteIfExists(Paths.get(DemoDataConst.YAML_LOCATE));
        Files.createFile(new File(DemoDataConst.YAML_LOCATE).toPath());
    }

    @AfterClass
    @SneakyThrows
    public static void deleteFilesAfter() {
        Files.deleteIfExists(Paths.get(DemoDataConst.BIN_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.BASE64_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.JSON_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.XML_LOCATE));
        Files.deleteIfExists(Paths.get(DemoDataConst.YAML_LOCATE));
    }

    @Test
    @TestCaseName("Run testDataBinClear for dataBinClear(session)")
    @SneakyThrows
    public void testDataBinClear() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isBinClear = this.dataEndpoint.dataBinClear(open);
        Assert.assertTrue(isBinClear);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDataBase64Clear for dataBase64Clear(session)")
    @SneakyThrows
    public void testDataBase64Clear() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isBase64Clear = this.dataEndpoint.dataBase64Clear(open);
        Assert.assertTrue(isBase64Clear);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDataJsonClear for dataJsonClear(session)")
    public void testDataJsonClear() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isJsonClear = this.dataEndpoint.dataJsonClear(open);
        Assert.assertTrue(isJsonClear);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDataXmlClear for dataXmlClear(session)")
    public void testDataXmlClear() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isXmlClear = this.dataEndpoint.dataXmlClear(open);
        Assert.assertTrue(isXmlClear);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDataYamlClear for dataYamlClear(session)")
    public void testDataYamlClear() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        final boolean isYamlClear = this.dataEndpoint.dataYamlClear(open);
        Assert.assertTrue(isYamlClear);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testExportDataBin for exportDataBin(session)")
    public void testExportDataBin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataBin(open);
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testExportDataBase64 for exportDataBase64(session)")
    public void testExportDataBase64() {

        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataBase64(open);
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testExportDataJson for exportDataJson(session)")
    public void testExportDataJson() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataJson(open);
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testExportDataXml for exportDataXml(session)")
    public void testExportDataXml() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataXml(open);
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testExportDataYaml for exportDataYaml(session)")
    public void testExportDataYaml() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataYaml(open);
        Assert.assertNotNull(domain);
        Assert.assertNotEquals(0, domain.getProjects().size());
        Assert.assertNotEquals(0, domain.getTasks().size());
        Assert.assertNotEquals(0, domain.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testImportDataBin for importDataBin(open)")
    public void testImportDataBin() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataJson(open);
        Assert.assertNotNull(domain);

        @NotNull final DomainDTO importData = this.dataEndpoint.importDataBin(open);
        Assert.assertNotNull(importData);
        Assert.assertNotEquals(0, importData.getProjects().size());
        Assert.assertNotEquals(0, importData.getTasks().size());
        Assert.assertNotEquals(0, importData.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testImportDataBase64 for importDataBase64(session)")
    public void testImportDataBase64() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataBase64(open);
        Assert.assertNotNull(domain);

        @NotNull final DomainDTO importData = this.dataEndpoint.importDataBase64(open);
        Assert.assertNotNull(importData);
        Assert.assertNotEquals(0, importData.getProjects().size());
        Assert.assertNotEquals(0, importData.getTasks().size());
        Assert.assertNotEquals(0, importData.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testImportDataJson for importDataJson(session)")
    public void testImportDataJson() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataJson(open);
        Assert.assertNotNull(domain);

        @NotNull final DomainDTO importData = this.dataEndpoint.importDataJson(open);
        Assert.assertNotNull(importData);
        Assert.assertNotEquals(0, importData.getProjects().size());
        Assert.assertNotEquals(0, importData.getTasks().size());
        Assert.assertNotEquals(0, importData.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testImportDataXml for importDataXml(session)")
    public void testImportDataXml() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataXml(open);
        Assert.assertNotNull(domain);

        @NotNull final DomainDTO importData = this.dataEndpoint.importDataXml(open);
        Assert.assertNotNull(importData);
        Assert.assertNotEquals(0, importData.getProjects().size());
        Assert.assertNotEquals(0, importData.getTasks().size());
        Assert.assertNotEquals(0, importData.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testImportDataYaml for importDataYaml(session)")
    public void testImportDataYaml() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.dataEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final DomainDTO domain = this.dataEndpoint.exportDataYaml(open);
        Assert.assertNotNull(domain);

        @NotNull final DomainDTO importData = this.dataEndpoint.importDataYaml(open);
        Assert.assertNotNull(importData);
        Assert.assertNotEquals(0, importData.getProjects().size());
        Assert.assertNotEquals(0, importData.getTasks().size());
        Assert.assertNotEquals(0, importData.getUsers().size());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

}