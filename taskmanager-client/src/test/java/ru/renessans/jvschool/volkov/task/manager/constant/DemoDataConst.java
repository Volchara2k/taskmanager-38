package ru.renessans.jvschool.volkov.task.manager.constant;

import org.jetbrains.annotations.NotNull;

public interface DemoDataConst {

    @NotNull
    String TASK_TITLE = "exceptions change";

    @NotNull
    String TASK_DESCRIPTION = "change the approach to exceptions";

    @NotNull
    String PROJECT_TITLE = "rethink view";

    @NotNull
    String PROJECT_DESCRIPTION = "rethink the view approach";

    @NotNull
    String USER_TEST_LOGIN = "test";

    @NotNull
    String USER_TEST_PASSWORD = "test";

    @NotNull
    String USER_DEFAULT_LOGIN = "user";

    @NotNull
    String USER_DEFAULT_PASSWORD = "user";

    @NotNull
    String USER_DEFAULT_FIRSTNAME = "user";

    @NotNull
    String USER_ADMIN_LOGIN = "admin";

    @NotNull
    String USER_ADMIN_PASSWORD = "admin";

    @NotNull
    String USER_MANAGER_LOGIN = "manager";

    @NotNull
    String USER_MANAGER_PASSWORD = "manager";

    @NotNull
    String BIN_LOCATE = "./data.bin";

    @NotNull
    String BASE64_LOCATE = "./data.base64";

    @NotNull
    String JSON_LOCATE = "./data.json";

    @NotNull
    String XML_LOCATE = "./data.xml";

    @NotNull
    String YAML_LOCATE = "./data.yaml";

}