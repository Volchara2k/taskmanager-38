package ru.renessans.jvschool.volkov.task.manager.endpoint;

import junitparams.JUnitParamsRunner;
import junitparams.naming.TestCaseName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import ru.renessans.jvschool.volkov.task.manager.constant.DemoDataConst;
import ru.renessans.jvschool.volkov.task.manager.marker.IntegrationImplementation;

import java.util.Collection;
import java.util.UUID;

@RunWith(value = JUnitParamsRunner.class)
@Category(IntegrationImplementation.class)
public final class TaskEndpointTest {

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @Test
    @TestCaseName("Run testAddTask for addTask(session, random, random)")
    public void testAddTask() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);

        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO isAddTask = this.taskEndpoint.addTask(
                open, title, description
        );
        Assert.assertNotNull(isAddTask);
        Assert.assertEquals(title, isAddTask.getTitle());
        Assert.assertEquals(description, isAddTask.getDescription());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testUpdateTaskByIndex for updateTaskByIndex(session, 0, random, random)")
    public void testUpdateTaskByIndex() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final TaskDTO updateTask =
                this.taskEndpoint.updateTaskByIndex(open, 0, title, description);
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(title, updateTask.getTitle());
        Assert.assertEquals(description, updateTask.getDescription());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testUpdateTaskById for updateTaskById(session, id, random, random)")
    public void testUpdateTaskById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final TaskDTO getTask = this.taskEndpoint.getTaskByIndex(open, 0);
        Assert.assertNotNull(getTask);

        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final String newDescription = UUID.randomUUID().toString();
        Assert.assertNotNull(newDescription);
        @Nullable final TaskDTO updateTask = this.taskEndpoint.updateTaskById(
                open, getTask.getId(), title, description
        );
        Assert.assertNotNull(updateTask);
        Assert.assertEquals(getTask.getId(), updateTask.getId());
        Assert.assertEquals(getTask.getUserId(), updateTask.getUserId());
        Assert.assertEquals(title, updateTask.getTitle());
        Assert.assertEquals(description, updateTask.getDescription());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskById for deleteTaskById(session, id)")
    public void testDeleteTaskById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_TEST_LOGIN, DemoDataConst.USER_TEST_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        final int deleteFlag = this.taskEndpoint.deleteTaskById(open, addTask.getId());
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskByIndex for deleteTaskByIndex(session, 0)")
    public void testDeleteTaskByIndex() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);
        @NotNull final TaskDTO getTask = this.taskEndpoint.getTaskByIndex(open, 0);
        Assert.assertNotNull(getTask);

        final int deleteFlag = this.taskEndpoint.deleteTaskByIndex(open, 0);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteTaskByTitle for deleteTaskByTitle(session, title)")
    public void testDeleteTaskByTitle() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        final int deleteFlag = this.taskEndpoint.deleteTaskByTitle(open, addTask.getTitle());
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testDeleteAllTasks for deleteAllTasks(session)")
    public void testDeleteAllTasks() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_DEFAULT_LOGIN, DemoDataConst.USER_DEFAULT_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        final int deleteFlag = this.taskEndpoint.deleteAllTasks(open);
        Assert.assertEquals(1, deleteFlag);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetTaskById for getTaskById(session, id)")
    public void testGetTaskById() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final TaskDTO getTask = this.taskEndpoint.getTaskById(open, addTask.getId());
        Assert.assertNotNull(getTask);
        Assert.assertEquals(addTask.getId(), getTask.getId());
        Assert.assertEquals(addTask.getUserId(), getTask.getUserId());
        Assert.assertEquals(addTask.getTitle(), getTask.getTitle());
        Assert.assertEquals(addTask.getDescription(), getTask.getDescription());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetTaskByIndex for getTaskByIndex(session, 0)")
    public void testGetTaskByIndex() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @NotNull final TaskDTO getTask = this.taskEndpoint.getTaskByIndex(open, 0);
        Assert.assertNotNull(getTask);
        Assert.assertEquals(open.getUserId(), getTask.getUserId());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetTaskByTitle for getTaskByTitle(session, title)")
    public void testGetTaskByTitle() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);
        @NotNull final String title = UUID.randomUUID().toString();
        Assert.assertNotNull(title);
        @NotNull final String description = UUID.randomUUID().toString();
        Assert.assertNotNull(description);
        @NotNull final TaskDTO addTask = this.taskEndpoint.addTask(open, title, description);
        Assert.assertNotNull(addTask);

        @Nullable final TaskDTO getTask = this.taskEndpoint.getTaskByTitle(open, addTask.getTitle());
        Assert.assertNotNull(getTask);
        Assert.assertEquals(addTask.getId(), getTask.getId());
        Assert.assertEquals(addTask.getUserId(), getTask.getUserId());
        Assert.assertEquals(addTask.getTitle(), getTask.getTitle());
        Assert.assertEquals(addTask.getDescription(), getTask.getDescription());
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

    @Test
    @TestCaseName("Run testGetAllTasks for getAllTasks(session)")
    public void testGetAllTasks() {
        Assert.assertNotNull(this.sessionEndpoint);
        Assert.assertNotNull(this.taskEndpoint);
        @NotNull final SessionDTO open = this.sessionEndpoint.openSession(
                DemoDataConst.USER_ADMIN_LOGIN, DemoDataConst.USER_ADMIN_PASSWORD
        );
        Assert.assertNotNull(open);

        @Nullable final Collection<TaskDTO> getAllTasks = this.taskEndpoint.getAllTasks(open);
        Assert.assertNotNull(getAllTasks);
        Assert.assertNotEquals(0, getAllTasks.size());
        final boolean isUserTasks = getAllTasks.stream().allMatch(entity -> open.getUserId().equals(entity.getUserId()));
        Assert.assertTrue(isUserTasks);
        @Nullable final SessionDTO closed = this.sessionEndpoint.closeSession(open);
        Assert.assertNotNull(closed);
    }

}