package ru.renessans.jvschool.volkov.task.manager.runner;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import ru.renessans.jvschool.volkov.task.manager.marker.RepositoryImplementation;
import ru.renessans.jvschool.volkov.task.manager.repository.CurrentSessionRepositoryTest;

@RunWith(Categories.class)
@Categories.IncludeCategory(RepositoryImplementation.class)
@Suite.SuiteClasses(
        {
                CurrentSessionRepositoryTest.class,
        }
)

public abstract class AbstractRepositoryImplementationRunner {
}