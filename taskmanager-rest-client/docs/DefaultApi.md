# DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProject**](DefaultApi.md#createProject) | **PUT** /api/projects/project/create | Create project
[**createProject_0**](DefaultApi.md#createProject_0) | **POST** /api/projects/project/create | Create project
[**createTask**](DefaultApi.md#createTask) | **PUT** /api/tasks/task/create | Create task
[**createTask_0**](DefaultApi.md#createTask_0) | **POST** /api/tasks/task/create | Create task
[**deleteProjectById**](DefaultApi.md#deleteProjectById) | **DELETE** /api/projects/project/{id} | Delete project by id
[**deleteTaskById**](DefaultApi.md#deleteTaskById) | **DELETE** /api/tasks/task/{id} | Delete task by id
[**editProject**](DefaultApi.md#editProject) | **PUT** /api/projects/project/edit/{id} | Edit project
[**editProject_0**](DefaultApi.md#editProject_0) | **POST** /api/projects/project/edit/{id} | Edit project
[**editTask**](DefaultApi.md#editTask) | **PUT** /api/tasks/task/edit/{id} | Edit task
[**editTask_0**](DefaultApi.md#editTask_0) | **POST** /api/tasks/task/edit/{id} | Edit task
[**getAllProjects**](DefaultApi.md#getAllProjects) | **GET** /api/projects | Get all projects
[**getAllTasks**](DefaultApi.md#getAllTasks) | **GET** /api/tasks | Get all tasks
[**getProjectById**](DefaultApi.md#getProjectById) | **GET** /api/projects/project/view/{id} | Get project by id
[**getTaskById**](DefaultApi.md#getTaskById) | **GET** /api/tasks/task/view/{id} | Get task by id


<a name="createProject"></a>
# **createProject**
> ProjectDTO createProject(body)

Create project

Field required

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.createProject(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createProject_0"></a>
# **createProject_0**
> ProjectDTO createProject_0(body)

Create project

Field required

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.createProject_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createProject_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createTask"></a>
# **createTask**
> TaskDTO createTask(body)

Create task

Field required

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.createTask(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createTask_0"></a>
# **createTask_0**
> TaskDTO createTask_0(body)

Create task

Field required

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.createTask_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createTask_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteProjectById"></a>
# **deleteProjectById**
> Integer deleteProjectById(id)

Delete project by id



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    Integer result = apiInstance.deleteProjectById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteProjectById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteTaskById"></a>
# **deleteTaskById**
> Integer deleteTaskById(id)

Delete task by id



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    Integer result = apiInstance.deleteTaskById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteTaskById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editProject"></a>
# **editProject**
> ProjectDTO editProject(body)

Edit project

Field required

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.editProject(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#editProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editProject_0"></a>
# **editProject_0**
> ProjectDTO editProject_0(body)

Edit project

Field required

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
ProjectDTO body = new ProjectDTO(); // ProjectDTO | 
try {
    ProjectDTO result = apiInstance.editProject_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#editProject_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**ProjectDTO**](ProjectDTO.md)|  |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editTask"></a>
# **editTask**
> TaskDTO editTask(body)

Edit task

Field required

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.editTask(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#editTask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="editTask_0"></a>
# **editTask_0**
> TaskDTO editTask_0(body)

Edit task

Field required

### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TaskDTO body = new TaskDTO(); // TaskDTO | 
try {
    TaskDTO result = apiInstance.editTask_0(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#editTask_0");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TaskDTO**](TaskDTO.md)|  |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllProjects"></a>
# **getAllProjects**
> List&lt;ProjectDTO&gt; getAllProjects()

Get all projects



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<ProjectDTO> result = apiInstance.getAllProjects();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAllProjects");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;ProjectDTO&gt;**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllTasks"></a>
# **getAllTasks**
> List&lt;TaskDTO&gt; getAllTasks()

Get all tasks



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<TaskDTO> result = apiInstance.getAllTasks();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAllTasks");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;TaskDTO&gt;**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getProjectById"></a>
# **getProjectById**
> ProjectDTO getProjectById(id)

Get project by id



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    ProjectDTO result = apiInstance.getProjectById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getProjectById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**ProjectDTO**](ProjectDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getTaskById"></a>
# **getTaskById**
> TaskDTO getTaskById(id)

Get task by id



### Example
```java
// Import classes:
//import ru.renessans.jvschool.volkov.task.manager.invoker.ApiException;
//import ru.renessans.jvschool.volkov.task.manager.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | 
try {
    TaskDTO result = apiInstance.getTaskById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getTaskById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |

### Return type

[**TaskDTO**](TaskDTO.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

